﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using LoanCalculator;
using NUnit.Framework;

namespace LoanCalculatorTest
{
    [TestFixture]
    public class MortgageCalculatorTest
    {

        [Test]
        public void MortgagePaymentsResults_30Years1To4DownPayment1InterestRate()
        {

            LoanTerms _loanTerms = new LoanTerms()
            {
                LoanAmount = 121875,
                AppraisedValue = 125000,
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400
            };

            _loanTerms.DownPayment = ((_loanTerms.AppraisedValue - _loanTerms.LoanAmount) / _loanTerms.AppraisedValue) * 100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(392, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest, 0));
            Assert.AreEqual(67, Math.Round(_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(112, Math.Round(_mortgageCalculator.MonthlyPayment.MortgageInsurance));
            Assert.AreEqual(570, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));


        }

        [Test]
        public void MortgagePaymentsResults_30Years5To9DownPayment1InterestRate()
        {
            
            LoanTerms _loanTerms = new LoanTerms()
            {
                LoanAmount = 115250,
                AppraisedValue = 125000,
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400
            };

            _loanTerms.DownPayment = ((_loanTerms.AppraisedValue - _loanTerms.LoanAmount) / _loanTerms.AppraisedValue) * 100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(371, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest, 0));
            Assert.AreEqual(67, Math.Round(_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(90, Math.Round(_mortgageCalculator.MonthlyPayment.MortgageInsurance));
            Assert.AreEqual(528, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));


        }

        [Test]
        public void MortgagePaymentsResults_30Years10To14DownPayment1InterestRate()
        {

            LoanTerms _loanTerms = new LoanTerms()  {
                LoanAmount = 244661,
                AppraisedValue = 279845,
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400,
                MortgagePercent = 18.6m
            };

            _loanTerms.DownPayment = (( _loanTerms.AppraisedValue - _loanTerms.LoanAmount ) / _loanTerms.AppraisedValue)*100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(787, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest,0));
            Assert.AreEqual(67, Math.Round(_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(126, Math.Round(_mortgageCalculator.MonthlyPayment.MortgageInsurance));
            Assert.AreEqual(980, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));

        }

        [Test]
        public void MortgagePaymentsResults_30Years15To19DownPayment1InterestRate()
        {

            LoanTerms _loanTerms = new LoanTerms()
            {
                LoanAmount = 244661,
                AppraisedValue = 279845,
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400,
                MortgagePercent = 18.6m
            };

            _loanTerms.DownPayment = ((_loanTerms.AppraisedValue - _loanTerms.LoanAmount) / _loanTerms.AppraisedValue) * 100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(787, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest, 0));
            Assert.AreEqual(67, Math.Round(_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(126, Math.Round(_mortgageCalculator.MonthlyPayment.MortgageInsurance));
            Assert.AreEqual(980, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));

        }

   
        [Test]
        public void TestMortgagePaymentsFivePercent()
        {

            LoanTerms _loanTerms = new LoanTerms()
            {
                LoanAmount = 244661,
                AppraisedValue = 279845,
                TermYears = 30,
                InterestRate = 5,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400                
            };

            _loanTerms.DownPayment = ((_loanTerms.AppraisedValue - _loanTerms.LoanAmount) / _loanTerms.AppraisedValue) * 100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(1313, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(126, Math.Round(_mortgageCalculator.MonthlyPayment.MortgageInsurance));
            Assert.AreEqual(1506, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));

        }

        [Test]
        public void TestMortgagePayments15Year()
        {

            LoanTerms _loanTerms = new LoanTerms()
            {
                LoanAmount = 244661,
                AppraisedValue = 279845,
                TermYears = 15,
                InterestRate = 5,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400               
            };

            _loanTerms.DownPayment = ((_loanTerms.AppraisedValue - _loanTerms.LoanAmount) / _loanTerms.AppraisedValue) * 100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(1935, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round (_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(104, Math.Round(_mortgageCalculator.MonthlyPayment.MortgageInsurance));
            Assert.AreEqual(2105, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));

        }

        [Test]
        public void TestMortgagePaymentsOnZero()
        {

            LoanTerms _loanTerms = new LoanTerms()
            {
                LoanAmount = 150000,
                AppraisedValue = 279845,
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyInsurance = 400,
                YearlyPropertyTax = 400
            };

            _loanTerms.DownPayment = ((_loanTerms.AppraisedValue - _loanTerms.LoanAmount) / _loanTerms.AppraisedValue) * 100;

            FixedRateLoanCalculator _mortgageCalculator = new FixedRateLoanCalculator();
            _mortgageCalculator.LoanTerms = _loanTerms;

            _mortgageCalculator.Calculate();

            Assert.AreEqual(482, Math.Round(_mortgageCalculator.MonthlyPayment.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(_mortgageCalculator.MonthlyPayment.TaxesAndInsurance));
            Assert.AreEqual(0, _mortgageCalculator.MonthlyPayment.MortgageInsurance);
            Assert.AreEqual(549, Math.Round(_mortgageCalculator.MonthlyPayment.TotalPayment));

        }
    }
}
