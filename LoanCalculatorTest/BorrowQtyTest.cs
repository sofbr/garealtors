﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using LoanCalculator;

namespace LoanCalculatorTest
{
    [TestFixture]
    public class BorrowQtyTest
    {

        [Test]
        public void TestConservative10DownPayment1InterestRate30Years0Doubts()
        {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 15896
            };

            Debts monthlyPayments = new Debts();

            LoanTerms loanTerms = new LoanTerms()
            {                
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyTax=400, YearlyPropertyInsurance=400,
                DownPayment = 10
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;
           
            calc.CalculatePayments(10);

            Assert.AreEqual(130492, Math.Round(calc.LoanPayments.DownPaymentAmount));
            Assert.AreEqual(1174428, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(1304920, Math.Round(calc.LoanPayments.PriceOfHome));

            Assert.AreEqual(3777, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(607, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(4451, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestConservative10DownPayment1InterestRate30Years1500Doubts()
        {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 15896
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 400,
                MonthlyAlimony = 400,
                MonthlyCreditCardPayments = 400,
                StudentLoans = 300
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyTax = 400,
                YearlyPropertyInsurance = 400,
                DownPayment = 10,                
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(10);

            Assert.AreEqual(123696, Math.Round(calc.LoanPayments.DownPaymentAmount));
            Assert.AreEqual(1113266, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(1236962, Math.Round(calc.LoanPayments.PriceOfHome));

            Assert.AreEqual(3581, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(575, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(4223, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestConservative20DownPayment1InterestRate30Years0Doubts()
        {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 3500
            };

            Debts monthlyPayments = new Debts();

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyTax = 400,
                YearlyPropertyInsurance = 400,
                DownPayment = 20               
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;
          
            calc.CalculatePayments(20);

            Assert.AreEqual(76431, Math.Round(calc.LoanPayments.DownPaymentAmount,0));
            Assert.AreEqual(305725, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(382156, Math.Round(calc.LoanPayments.PriceOfHome,0));

            Assert.AreEqual(983, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(1050, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestConservative20DownPayment1InterestRate30Years567Doubts()
        {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 3500
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans=200,
                MonthlyAlimony=200,
                RentalPropertyLoans=100,
                OtherPayments=67
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyTax = 400,
                YearlyPropertyInsurance = 400,
                DownPayment = 20
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(20);

            Assert.AreEqual(48683, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(194731, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(243414, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(626, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(693, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestConservative30DownPayment2InterestRate0Doubts() {

            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 1000, InvestmentIncome=1000, IncomeFromRental=500, OtherIncome=60                
            };

            Debts monthlyPayments = new Debts();

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 2,
                YearlyPropertyTax = 250,
                YearlyPropertyInsurance = 350,
                DownPayment = 30,                
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(30);

            Assert.AreEqual(83251, Math.Round(calc.LoanPayments.DownPaymentAmount,0));
            Assert.AreEqual(194253, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(277504, Math.Round(calc.LoanPayments.PriceOfHome,0));


            Assert.AreEqual(718, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(50, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(768, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestConservative30DownPayment2InterestRate456Doubts()
        {

            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 1000,
                InvestmentIncome = 1000,
                IncomeFromRental = 500,
                OtherIncome = 60
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 467
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 2,
                YearlyPropertyTax = 250,
                YearlyPropertyInsurance = 350,
                DownPayment = 30
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(30);

            Assert.AreEqual(46913, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(109463, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(156376, Math.Round(calc.LoanPayments.PriceOfHome, 0));


            Assert.AreEqual(405, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(50, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(455, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestConservativeCustom12DownPayment2_5InterestRate450Debts() {

            MonthlyIncome income = new MonthlyIncome()
            {
                WageIncome = 2547,
                IncomeFromRental = 123,
                InvestmentIncome = 49,
                OtherIncome = 1
            };

            Debts debts = new Debts()
            {
                AutoLoans = 345,
                MonthlyAlimony = 23,
                OtherPayments = 2,
                MonthlyCreditCardPayments = 45,
                RentalPropertyLoans = 12,
                StudentLoans = 23
            };

            LoanTerms terms = new LoanTerms()
            {
                InterestRate = 2.5m,
                TermYears = 30,
                YearlyPropertyTax=345, YearlyPropertyInsurance=200
            };

            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Conservative);

            calc.monthlyIncome = income;
            calc.MonthlyPayments = debts;
            calc.loanTerms = terms;

            calc.CalculatePayments(12);

            Assert.AreEqual(14765 , Math.Round(calc.LoanPayments.DownPaymentAmount,0));
            Assert.AreEqual(108280, calc.LoanPayments.LoanAmount);
            Assert.AreEqual(123045, Math.Round(calc.LoanPayments.PriceOfHome,0));

            Assert.AreEqual(428, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(45, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(56, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(529, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestAggresive10DownPayment1InterestRate0Debts() {

            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 15896
            };

            Debts monthlyPayments = new Debts();

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyTax = 400,
                YearlyPropertyInsurance = 400,
                DownPayment = 10
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(10);

            Assert.AreEqual(154148, Math.Round(calc.LoanPayments.DownPaymentAmount,0));
            Assert.AreEqual(1387336, Math.Round(calc.LoanPayments.LoanAmount,0));
            Assert.AreEqual(1541484, Math.Round(calc.LoanPayments.PriceOfHome,0));

            Assert.AreEqual(4462, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(717, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(5246, Math.Round(calc.LoanPayments.TotalPayment));
        
        }

        [Test]
        public void TestAggresive10DownPayment1_6InterestRate345Debts()
        {

            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 15896
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans=50,
                MonthlyAlimony=20,
                OtherPayments=50,
                RentalPropertyLoans=50,
                MonthlyCreditCardPayments=75,
                StudentLoans=100
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1,
                YearlyPropertyTax = 400,
                YearlyPropertyInsurance = 400,
                DownPayment = 10
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(10);

            Assert.AreEqual(154148, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(1387336, Math.Round(calc.LoanPayments.LoanAmount, 0));
            Assert.AreEqual(1541484, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(4462, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(67, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(717, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(5246, Math.Round(calc.LoanPayments.TotalPayment));

        }

        [Test]
        public void TestAggresive20DownPayment1_2InterestRate234Debts() {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 3629
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 1233,
                MonthlyAlimony = 0,
                OtherPayments = 0,
                RentalPropertyLoans = 45,
                MonthlyCreditCardPayments = 0,
                StudentLoans = 0
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1.2m,
                YearlyPropertyTax = 0,
                YearlyPropertyInsurance = 0,
                DownPayment = 20
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(20);

            Assert.AreEqual(18599, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(74395, Math.Round(calc.LoanPayments.LoanAmount, 0));
            Assert.AreEqual(92994, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(246, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(246, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestAggresive30DownPayment2_2InterestRate291Debts() {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 1278
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 256,
                MonthlyAlimony = 0,
                OtherPayments = 0,
                RentalPropertyLoans = 45,
                MonthlyCreditCardPayments = 0,
                StudentLoans = 0
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1.2m,
                YearlyPropertyTax = 0,
                YearlyPropertyInsurance = 0,
                DownPayment = 20
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(30);

            Assert.AreEqual(30534, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(71246, Math.Round(calc.LoanPayments.LoanAmount, 0));
            Assert.AreEqual(101780, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(236, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(236, Math.Round(calc.LoanPayments.TotalPayment));
        
        }

        [Test]
        public void TestAggresive30DownPayment2_2InterestRate0Debts()
        {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 1278
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 256,
                MonthlyAlimony = 0,
                OtherPayments = 0,
                RentalPropertyLoans = 45,
                MonthlyCreditCardPayments = 0,
                StudentLoans = 0
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 1.2m,
                YearlyPropertyTax = 0,
                YearlyPropertyInsurance = 0,
                DownPayment = 20
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(30);

            Assert.AreEqual(30534, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(71246, Math.Round(calc.LoanPayments.LoanAmount, 0));
            Assert.AreEqual(101780, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(236, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(236, Math.Round(calc.LoanPayments.TotalPayment));

        }

        [Test]
        public void TestAggresiveCustom12_5DownPayment2_2InterestRate301Debts() {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 1278
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 256,
                MonthlyAlimony = 0,
                OtherPayments = 0,
                RentalPropertyLoans = 45,
                MonthlyCreditCardPayments = 0,
                StudentLoans = 0
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 2.2m,
                YearlyPropertyTax = 0,
                YearlyPropertyInsurance = 0
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(12.5m);

            Assert.AreEqual(6961, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(48728, Math.Round(calc.LoanPayments.LoanAmount, 0));
            Assert.AreEqual(55689, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(185, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(25, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(210, Math.Round(calc.LoanPayments.TotalPayment));
        }

        [Test]
        public void TestNotAuthorizedLoan()
        {
            MonthlyIncome monthlyIncome = new MonthlyIncome()
            {
                WageIncome = 1278
            };

            Debts monthlyPayments = new Debts()
            {
                AutoLoans = 869,
                MonthlyAlimony = 0,
                OtherPayments = 0,
                RentalPropertyLoans = 45,
                MonthlyCreditCardPayments = 0,
                StudentLoans = 0
            };

            LoanTerms loanTerms = new LoanTerms()
            {
                TermYears = 30,
                InterestRate = 2.2m,
                YearlyPropertyTax = 0,
                YearlyPropertyInsurance = 0
            };


            EstimateCalculator calc = new EstimateCalculator(eEstimateType.Aggresive);

            calc.monthlyIncome = monthlyIncome;
            calc.MonthlyPayments = monthlyPayments;
            calc.loanTerms = loanTerms;

            calc.CalculatePayments(12.5m);

            Assert.AreEqual(0, Math.Round(calc.LoanPayments.DownPaymentAmount, 0));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.LoanAmount, 0));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.PriceOfHome, 0));

            Assert.AreEqual(0, Math.Round(calc.LoanPayments.PrincipalAndInterest));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.TaxesAndInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.MortgageInsurance));
            Assert.AreEqual(0, Math.Round(calc.LoanPayments.TotalPayment));
        }




   
    }
}
