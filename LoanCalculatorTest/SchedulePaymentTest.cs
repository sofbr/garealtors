﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using LoanCalculator;
using NUnit.Framework;

namespace LoanCalculatorTest
{     
    [TestFixture]
    public class SchedulePaymentTest
    {        
        [Test]
        public void Years30Interest1Loan198000()
        {

            SchedulePaymentTable schedule = new SchedulePaymentTable();
            FixedSchedulePaymentCalculator calc = new FixedSchedulePaymentCalculator() { InterestRate = 1, LoanAmount = 198000, TermMonths = 360 };
            schedule.Calculator = calc;

            schedule.BuildSchedulePayment();

            var table=schedule.SchedulePayments;

            Assert.AreEqual(360, schedule.SchedulePayments.Count);
            Assert.AreEqual(637, Math.Round(table[0].MonthlyPayment, 0));
 
            //Test first schedule row           
            Assert.AreEqual(165, Math.Round(table[0].InterestPaid));
            Assert.AreEqual(472, Math.Round(table[0].PrincipalPaid));
            Assert.AreEqual(197528, Math.Round(table[0].Balance));
            Assert.AreEqual(165, Math.Round(table[0].CumulativeInterest));
            //Test last schedule row            
            Assert.AreEqual(1, Math.Round(table[359].InterestPaid));
            Assert.AreEqual(636, Math.Round(table[359].PrincipalPaid));
            Assert.AreEqual(0, Math.Round(table[359].Balance));
            Assert.AreEqual(31265, Math.Round(table[359].CumulativeInterest));
        }

        [Test]
        public void Years15Interest1Loan198000(){
            SchedulePaymentTable schedule = new SchedulePaymentTable();
            FixedSchedulePaymentCalculator calc = new FixedSchedulePaymentCalculator() { InterestRate = 1, TermMonths = 180, LoanAmount = 198000 };
            schedule.Calculator = calc;

            schedule.BuildSchedulePayment();

            var table=schedule.SchedulePayments;

            Assert.AreEqual(180, schedule.SchedulePayments.Count);
            Assert.AreEqual(1185, Math.Round(table[0].MonthlyPayment, 0));

            //Test first schedule row           
            Assert.AreEqual(165, Math.Round(table[0].InterestPaid));
            Assert.AreEqual(1020,Math.Round(table[0].PrincipalPaid));
            Assert.AreEqual(196980, Math.Round(table[0].Balance));
            Assert.AreEqual(165, Math.Round(table[0].CumulativeInterest));
            //Test last schedule row          
            Assert.AreEqual(1, Math.Round(table[179].InterestPaid));
            Assert.AreEqual(1184, Math.Round(table[179].PrincipalPaid));
            Assert.AreEqual(0, Math.Round(table[179].Balance));
            Assert.AreEqual(15303, Math.Round(table[179].CumulativeInterest));


        }
    }
}
