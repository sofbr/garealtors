﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using LoanCalculator;
using NUnit.Framework;

namespace LoanCalculatorTest
{
	[TestFixture]
	public class PMTTest
	{

        [Test]
        public void TestA_InterestRate1Years30Loan1174428() {
            decimal A = Calculator.GetA(1174428, 1, 360);
        
            Assert.AreEqual(3777, Math.Round(A));           
        }


        [Test]
        public void TestA_InterestRate1Years15Loan687871()
        {
            decimal A = Calculator.GetA(687871, 1, 180);

            Assert.AreEqual(4117, Math.Round(A));
        }

        [Test]
        public void TestP_InterestRate1Years30MonthlyPayments3777() {
            decimal P = Calculator.GetP(3777.424587m, 1, 360);

            Assert.AreEqual(1174428, Math.Round(P));
        }

        [Test]
        public void TestA_InterestRate5Years30Loan749244() {
            decimal A = Calculator.GetA(749244, 5, 360);

            Assert.AreEqual(4022, Math.Round(A)); 
        }

        [Test]
        public void TestP_InterestRate5Year30MonthlyPayments4022()
        {
            decimal P = Calculator.GetP(4022.103801m, 5, 360);

            Assert.AreEqual(749244, Math.Round(P));
        }

	}
}
