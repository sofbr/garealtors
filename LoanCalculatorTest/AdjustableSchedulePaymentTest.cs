﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using LoanCalculator;

namespace LoanCalculatorTest
{
    [TestFixture]
    public class AdjustableSchedulePaymentTest
    {   

        [Test]
        public void TestIndex0Margin1Term360()
        {

            SchedulePaymentTable schedule = new SchedulePaymentTable();
            AdjustableScheduleCalculator calc = new AdjustableScheduleCalculator()
            {
                LoanAmount = 200000m,
                MonthsBeforeFirstAdjustment =6,
                InterestRate = 1,
                MonthsBetweenRateAdjustments = 3,
                MaximumRateAdjustment = 20,
                MinimumRate = 0,
                MaximumRate = 50,
                Margin = 1m,
                IndexRate = 0,
                PredictedChangeInRate = eChangeRate.InterestRatesWillIncrease,
                MonthsBetweenIndexChange = 2,
                IndexRateChangePerAdjustment = 1,
                TermMonths = 360
            };

            schedule.Calculator = calc;

            schedule.BuildSchedulePayment();

            var table = schedule.SchedulePayments;

            Assert.AreEqual(643, Math.Round( table[0].MonthlyPayment));
            Assert.AreEqual(199523, Math.Round( table[0].Balance));
            Assert.AreEqual(477, Math.Round(table[0].PrincipalPaid));
            Assert.AreEqual(167, Math.Round(table[0].InterestPaid));

            Assert.AreEqual(949, Math.Round( table[6].MonthlyPayment));
            Assert.AreEqual(196842, Math.Round(table[6].Balance));
            Assert.AreEqual(292, Math.Round(table[6].PrincipalPaid));
            Assert.AreEqual(1651, Math.Round(table[6].CumulativeInterest));

            Assert.AreEqual(8051, Math.Round(table[359].MonthlyPayment));
            Assert.AreEqual(0, Math.Round(table[359].Balance));
            Assert.AreEqual(7729, Math.Round(table[359].PrincipalPaid));
            Assert.AreEqual(2302512, Math.Round(table[359].CumulativeInterest));



        }

        [Test]
        public void TestIndex0Margin0Term360()
        {

            SchedulePaymentTable schedule = new SchedulePaymentTable();
            AdjustableScheduleCalculator calc = new AdjustableScheduleCalculator()
            {
                LoanAmount = 198000,                
                MonthsBeforeFirstAdjustment = 1,
                InterestRate = 1,
                MonthsBetweenRateAdjustments = 1,
                MaximumRateAdjustment = 0,
                MinimumRate = 0,
                MaximumRate = 0,
                Margin = 0m,
                IndexRate = 0,
                PredictedChangeInRate = eChangeRate.InterestRatesWillIncrease,
                MonthsBetweenIndexChange = 1,
                IndexRateChangePerAdjustment = 0,
                TermMonths = 360
            };

            schedule.Calculator = calc;

            schedule.BuildSchedulePayment();         



        }
    }
}
