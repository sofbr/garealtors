﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoanCalculator;
using System.Drawing;

namespace GARealtors
{
    public partial class MortgagePayments : System.Web.UI.UserControl
    {

        List<string> errors;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Calculate() {

            LoanTerms terms = new LoanTerms()
            {
                LoanAmount = decimal.Parse(txtLoanAmount.Text),
                AppraisedValue = decimal.Parse(txtAppraisedValue.Text),
                TermYears = int.Parse(txtTermYears.Text),
                YearlyPropertyTax = decimal.Parse(txtPropertyTax.Text),
                YearlyPropertyInsurance = decimal.Parse(txtPropertyInsurance.Text),
                InterestRate = decimal.Parse(txtInterestRate.Text)
            };

            terms.DownPayment = ((terms.AppraisedValue - terms.LoanAmount) / terms.AppraisedValue) * 100;
            
            FixedRateLoanCalculator calc = new FixedRateLoanCalculator();

            calc.LoanTerms = terms;

            calc.Calculate();
                       
            lblPrincipalAndInterest.Text = String.Format("{0:C0}",calc.MonthlyPayment.PrincipalAndInterest);
            lblTaxesAndInsurance.Text = string.Format("{0:C0}", calc.MonthlyPayment.TaxesAndInsurance);
            lblMortgageInsurance.Text = string.Format("{0:C0}", calc.MonthlyPayment.MortgageInsurance);
            lblTotalPayment.Text = string.Format("{0:C0}", calc.MonthlyPayment.TotalPayment);

        }
        

        protected SchedulePaymentTable GetSchedule()
        {

            SchedulePaymentTable _schedule = new SchedulePaymentTable();
            
            FixedSchedulePaymentCalculator calc = 
            new FixedSchedulePaymentCalculator(){ 
                 InterestRate=decimal.Parse(txtInterestRate.Text),
                 LoanAmount=decimal.Parse(txtLoanAmount.Text), TermMonths = int.Parse(txtTermYears.Text) * 12 
            };

            _schedule.Calculator = calc;

            _schedule.BuildSchedulePayment();

            var table = _schedule.SchedulePayments;

            return _schedule;

        }

        protected void BuildSchedulePayments() {

            var table = GetSchedule();

            grdSchedulePayment.DataSource = table.SchedulePayments;
            grdSchedulePayment.DataBind();


        }

        protected void CalculateChart() {

            var table = GetSchedule();

            decimal[] principalPayment=new decimal[5];
            decimal[] interestPayment=new decimal[5];

            foreach (SchedulePayment payment in table.SchedulePayments) {

                if (payment.Month % 72 == 0) {
                    principalPayment[(payment.Month / 72) - 1] = payment.PrincipalPaid;
                    interestPayment[(payment.Month / 72) - 1] = payment.InterestPaid;
                }
          
            }
          
            double[] debts = { 5, 10, 15, 20, 30 };

            MortgageChart.Series[0].Points.DataBindXY(debts, principalPayment);
            MortgageChart.Series[1].Points.DataBindXY(debts, interestPayment);


            MortgageChart.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.StackedColumn;
            MortgageChart.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.StackedColumn;

            MortgageChart.Series[0].LegendText = "Principal";
            MortgageChart.Series[1].LegendText = "Interest";

            MortgageChart.ChartAreas[0].AxisX.Title = "Years";
            MortgageChart.ChartAreas[0].AxisY.Title = "Monthly payment ($)";

        }

        protected void ActiveTab(string tab)
        {

            TabInputs.Attributes["class"] = TabInputs.ID == tab ? "cell active" : "cell inactive";
            TabResults.Attributes["class"] = TabResults.ID == tab ? "cell active" : "cell inactive";
            TabGraph.Attributes["class"] = TabGraph.ID == tab ? "cell active" : "cell inactive";
            TabTables.Attributes["class"] = TabTables.ID == tab ? "cell active" : "cell inactive";
            TabHelp.Attributes["class"] = TabHelp.ID == tab ? "cell active" : "cell inactive";

        }
       

        protected void ShowInputs() {
            mvMain.SetActiveView(vwInputs);
            ActiveTab(TabInputs.ID);
        }

        protected void ShowResult() {

            Page.Validate();

            if (!Page.IsValid) return;

            Calculate();

            mvMain.SetActiveView(vwResults);
            ActiveTab(TabResults.ID);
        }

        protected void ShowGraphs() {

            Page.Validate();

            if (!Page.IsValid) return;

            mvMain.SetActiveView(vwGraphs);
            ActiveTab(TabGraph.ID);

            CalculateChart();
        }
       
        protected void ShowHelp() {
            mvMain.SetActiveView(vwHelp);
            ActiveTab(TabHelp.ID);
        }

        protected void btnResults_Click(object sender, EventArgs e)
        {            
            ShowResult();
        }

        protected void lnkInputs_Click(object sender, EventArgs e)
        {
            ShowInputs();
        }

        protected void lnkGraph_Click(object sender, EventArgs e)
        {
            ShowGraphs();
        }

        protected void ShowTables() {

            BuildSchedulePayments();
            mvMain.SetActiveView(vwTables);
            ActiveTab(TabTables.ID);
        }

        protected void lnkHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        protected void lnkResults_Click(object sender, EventArgs e)
        {         
            ShowResult();
        }

        protected void lnlTable_Click(object sender, EventArgs e)
        {
            ShowTables();
        }

        protected void ValidateLoanTerms() { 
                                  
            decimal loanAmount,appraisedValue,interestRate,tax,insurance;
            int termYears;        

            lblLoanAmount.CssClass = "";
            lblAppraisedValue.CssClass = "";
            lblInterestRate.CssClass = "";
            lblTermYears.CssClass = "";
            lblPropertyTax.CssClass = "";
            lblPropertyInsurance.CssClass = "";


            if (!Validator.validate(txtLoanAmount.Text, 1m, 1000000000m, out loanAmount)) {
                errors.Add("Loan amount must be a number between 1 and 1,000,000,000.");
                lblLoanAmount.CssClass = "LabelError";
            }

            if (!Validator.validate(txtAppraisedValue.Text, 1m, 1000000000m, out appraisedValue)) {
                errors.Add("Appraised value must be a number between 1 and 1,000,000,000.");
                lblAppraisedValue.CssClass = "LabelError";
            }

            if (!Validator.validate(txtTermYears.Text, 1, 40, out termYears)) {
                errors.Add("Term (years) must be a whole number between 1 and 40.");
                lblTermYears.CssClass = "LabelError";
            }

            if (!Validator.validate(txtInterestRate.Text, 0, 50, out interestRate)) {
                errors.Add("Interest rate must be a number between 0.000 and 50.000.");
                lblInterestRate.CssClass = "LabelError";
            }             
            if (!Validator.validate(txtPropertyTax.Text, 0, 100000, out tax)) {
                errors.Add("Yearly property tax must be a number between 0 and 100,000.");
                lblPropertyTax.CssClass = "LabelError";
            }
            if(!Validator.validate(txtPropertyInsurance.Text,0,100000,out insurance)){
                errors.Add("Yearly property insurance must be a number between 0 and 100,000.");
                lblPropertyInsurance.CssClass = "LabelError";
            }         
        

        }

        protected void BorrowValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            errors = new List<string>();

            ValidateLoanTerms();
            if(errors.Count>0) args.IsValid = false;

            lstErrores.DataSource = errors;
            lstErrores.DataBind();
           
        }
    }
}