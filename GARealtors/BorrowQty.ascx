﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/BorrowQTY.ascx.cs" Inherits="GARealtors.BorrowQty" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

    <div id="CalculatorWrapper">

        <div id="CalculatorTitle">
            <span>How much can I borrow?</span>
        </div>

        <div class="CalculatorContent">

            <section id="Tabs" class="table">

                <div id="TabInputs" class="cell active" runat="server" >
                    <asp:LinkButton ID="lnkInputs" Text="Inputs" runat="server" onclick="lnkInputs_Click" />
                </div>        
                <div id="TabResults" class="cell inactive" runat="server" >
                    <asp:LinkButton ID="lnlResults" Text="Results" runat="server" onclick="lnlResults_Click" />
                </div>
                <div id="TabGraph" class="cell inactive" runat="server" >
                    <asp:LinkButton ID="lnkGraph" Text="Graph" runat="server" onclick="lnkGraph_Click" />
                </div>
                <div id="TabHelp" class="cell inactive" runat="server"  >
                    <asp:LinkButton ID="lnkHelp" Text="Help" runat="server" onclick="lnkHelp_Click" />
                </div>

            </section>
    
            <asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">
        
                <asp:View ID="vwInputs" runat="server">
            
                    <div id="Inputs" class="Inputs" runat="server">

                        <div>
                            <asp:ValidationSummary ID="valSummary" EnableClientScript="true" ShowMessageBox="false" DisplayMode="List" ShowSummary="true" runat="server" />
                            <asp:CustomValidator runat="server" ID="BorrowValidator" onservervalidate="BorrowValidator_ServerValidate" />
                        </div>

                        <asp:ListView ID="lstErrores" runat="server">
                            <LayoutTemplate>
                                <div class="isa_error">
                                    <span>Please fix following errors</span>
                                    <ul>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </ul>
                                </div>
                            </LayoutTemplate>           
                            <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>                    
                        </asp:ListView>

                        <section class="table InputFields"> 
                            <div class="header">
                                <div class="cell col1 InputCategory">Monthly income</div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblWages" Text="Wages before taxes or deductions" runat="server" /></div>
                                <div class="cell col2">
                                    <asp:TextBox id="txtWages_Income"  runat="server" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblInvestment" Text="Investment income before taxes" runat="server" /></div>
                                <div class="cell col2">
                                    <asp:TextBox id="txtInvestment_Income"  runat="server" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblRental" Text="Income from rental properties" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox id="txtRental_Income"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblOtherIncome" Text="Other Income" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox id="txtOther_Income"  runat="server" /></div>
                            </div>            
                        </section>    

                        <section class="table InputFields">
                            <div class="header">
                                <div class="cell col1 InputCategory">Monthly Payments *</div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblAutoLoans" Text="Auto loans" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtAutoLoans_Payment"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblStudentLoans" Text="Students loans" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtStudentLoans_Payments" runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblRentalPayment" Text="Rental property loans ($0 if refinancing)" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtRental_Payment"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblOtherPayment" Text="Other payments" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtOther_Payment"  runat="server" /></div>
                            </div>
                            <div class="footer">
                                <div class="cell col1">* Include only loans that won't be paid off in 10 months</div>
                            </div>
                        </section>

                        <section class="table InputFields">
                            <div class="header">
                                Other debts
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblAlimony" Text="Monthly alimony, child support or other" runat="server"></asp:Label></div>
                                <div class="cell col2"><asp:TextBox ID="txtAlimony_OtherDebts"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblCreditCard" Text="Monthly credit card payments" runat="server"></asp:Label></div>
                                <div class="cell col2"><asp:TextBox ID="txtMonthlyCreditCard_OtherDebts"  runat="server" /></div>
                            </div>
                        </section>
    
                        <section class="table InputFields">
                            <div class="header">
                                <div class="cell col1 InputCategory">Loan terms you desire</div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblInterestRate" Text="Interest rate" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtInterestRate"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblTermYears" Text="Terms (years)" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtTerms"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblDownPayment" Text="DownPayment (% of price)" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtDownPayment" runat="server" /></div>
                            </div>       
                        </section>
    
                        <section class="table InputFields">
                            <div class="header">
                                <div class="cell col1 InputCategory">Taxes & insurance you except</div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblYearlyTax" Text="Yearly property tax" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtYearlyPropertyTax"  runat="server" /></div>
                            </div>
                            <div class="row">
                                <div class="cell col1"><asp:Label ID="lblYearlyInsurance" Text="Yearly property insurance" runat="server" /></div>
                                <div class="cell col2"><asp:TextBox ID="txtYearlyPropertyInsurance"  runat="server" /></div>
                            </div>
                        </section>       
 
                        <asp:Button ID="btnResults" CssClass="Button" Text="Results..." runat="server" onclick="btnResults_Click" />

                    </div>

                </asp:View>
        
                <asp:View ID="vwResults" runat="server">

                    <div id="Results" class="Results" runat="server">
                        <div id="Note">
                            <p>NOTE: Please be sure that you've entered a monthly (not yearly) income figure in the field labeled "Wages Before Taxes or Deductions" and all other fields relating to income.           
                            </p>
                            <p>
                            The amount you can borrow, based upon various down payments, including the down payment you indicated:
                            </p>
                        </div>
    
                        <table id="ConservativeEstimate">
                            <thead>
                                <tr><td colspan="5">A conservative estimate</td></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Percent down</td>
                                    <td>10.00%</td>
                                    <td>20.00%</td>
                                    <td>30.00%</td>
                                    <td><asp:Label ID="lblCCustomPercent" runat="server" /></td>
                                
                                </tr>
                                <tr>
                                    <td>Down payment amount</td>
                                    <td><asp:Label ID="lblCDownPayment10" runat="server" /></td>
                                    <td><asp:Label ID="lblCDownPayment20" runat="server" /></td>
                                    <td><asp:Label ID="lblCDownPayment30" runat="server" /></td>
                                    <td><asp:Label ID="lblCDownPaymentCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Loan amount</td>
                                    <td><asp:Label ID="lblCLoanAmount10" runat="server" /></td>
                                    <td><asp:Label ID="lblCLoanAmount20" runat="server" /></td>
                                    <td><asp:Label ID="lblCLoanAmount30" runat="server" /></td>
                                    <td><asp:Label ID="lblCLoanAmountCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Price home</td>
                                    <td><asp:Label ID="lblCPriceHome10" runat="server" /></td>
                                    <td><asp:Label ID="lblCPriceHome20" runat="server" /></td>
                                    <td><asp:Label ID="lblCPriceHome30" runat="server" /></td>
                                    <td><asp:Label ID="lblCPriceHomeCustom" runat="server" /></td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="ConservativeMonthlyPayment">
                            <thead>
                                <tr><td colspan="5">Your Future Monthly Payment</td></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Principal and interest</td>
                                    <td><asp:Label ID="lblCPrincipalPayment10" runat="server" /></td>
                                    <td><asp:Label ID="lblCPrincipalPayment20" runat="server" /></td>
                                    <td><asp:Label ID="lblCPrincipalPayment30" runat="server" /></td>
                                    <td><asp:Label ID="lblCPrincipalPaymentCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Taxes and insurance</td>
                                    <td><asp:Label ID="lblCTaxesAndIns10" runat="server" /></td>
                                    <td><asp:Label ID="lblCTaxesAndIns20" runat="server" /></td>
                                    <td><asp:Label ID="lblCTaxesAndIns30" runat="server" /></td>
                                    <td><asp:Label ID="lblCTaxesAndInsCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Mortgage insurance</td>
                                    <td><asp:Label ID="lblCMortgage10" runat="server" /></td>
                                    <td><asp:Label ID="lblCMortgage20" runat="server" /></td>
                                    <td><asp:Label ID="lblCMortgage30" runat="server" /></td>
                                    <td><asp:Label ID="lblCMortgageCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Total monthly payment</td>
                                    <td><asp:Label ID="lblCTotalPayment10" runat="server" /></td>
                                    <td><asp:Label ID="lblCTotalPayment20" runat="server" /></td>
                                    <td><asp:Label ID="lblCTotalPayment30" runat="server" /></td>
                                    <td><asp:Label ID="lblCTotalPaymentCustom" runat="server" /></td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="AggresiveEstimate">
                            <thead>
                                <tr><td colspan="5">An aggresive estimate</td></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Percent down</td>
                                    <td>10.00%</td>
                                    <td>20.00%</td>
                                    <td>30.00%</td>
                                    <td><asp:Label ID="lblACustomPercent" Text="lblACustomPercent" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Down payment amount</td>
                                    <td><asp:Label ID="lblADownPayment10" runat="server" /></td>
                                    <td><asp:Label ID="lblADownPayment20" runat="server" /></td>
                                    <td><asp:Label ID="lblADownPayment30" runat="server" /></td>
                                    <td><asp:Label ID="lblADownPaymentCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Loan amount</td>
                                    <td><asp:Label ID="lblALoanAmount10" runat="server" /></td>
                                    <td><asp:Label ID="lblALoanAmount20" runat="server" /></td>
                                    <td><asp:Label ID="lblALoanAmount30" runat="server" /></td>
                                    <td><asp:Label ID="lblALoanAmountCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Price home</td>
                                    <td><asp:Label ID="lblAPriceHome10" runat="server" /></td>
                                    <td><asp:Label ID="lblAPriceHome20" runat="server" /></td>
                                    <td><asp:Label ID="lblAPriceHome30" runat="server" /></td>
                                    <td><asp:Label ID="lblAPriceHomeCustom" runat="server" /></td>
                                </tr>
                            </tbody>
                        </table>

                         <table id="AggresiveMonthlyPayment">
                            <thead>
                                <tr><td colspan="5">Your Future Monthly Payment</td></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Principal and interest</td>
                                    <td><asp:Label ID="lblAPrincipalPayment10" runat="server" /></td>
                                    <td><asp:Label ID="lblAPrincipalPayment20" runat="server" /></td>
                                    <td><asp:Label ID="lblAPrincipalPayment30" runat="server" /></td>
                                    <td><asp:Label ID="lblAPrincipalPaymentCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Taxes and insurance</td>
                                    <td><asp:Label ID="lblATaxesAndIns10" runat="server" /></td>
                                    <td><asp:Label ID="lblATaxesAndIns20" runat="server" /></td>
                                    <td><asp:Label ID="lblATaxesAndIns30" runat="server" /></td>
                                    <td><asp:Label ID="lblATaxesAndInsCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Mortgage insurance</td>
                                    <td><asp:Label ID="lblAMortgage10" runat="server" /></td>
                                    <td><asp:Label ID="lblAMortgage20" runat="server" /></td>
                                    <td><asp:Label ID="lblAMortgage30" runat="server" /></td>
                                    <td><asp:Label ID="lblAMortgageCustom" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Total monthly payment</td>
                                    <td><asp:Label ID="lblATotalPayment10" runat="server" /></td>
                                    <td><asp:Label ID="lblATotalPayment20" runat="server" /></td>
                                    <td><asp:Label ID="lblATotalPayment30" runat="server" /></td>
                                    <td><asp:Label ID="lblATotalPaymentCustom" runat="server" /></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </asp:View>
        
                <asp:View ID="vwGraph" runat="server">
                    
                    <div id="Graphs" class="Chart" runat="server">

                        <div id="GraphTitle">Loan amount vs debt</div>

                        <div id="TheGraph">
        
                            <asp:Chart ID="BorrowChart" runat="server" Width="550px">
                                <series>
                                    <asp:Series ChartType="Line" Name="Series1" Legend="Legend1">
                                    </asp:Series>
                                </series>            
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
        
                        </div>   

                        <div id="GraphFooter">
                            <p>This graph illustrates the loan amount for which you may be able to qualify. The higher the amount of your monthly debt, the lower the amount of the loan you can obtain. The data shown in the graph represents the conservative scenario with a 35% down payment.</p>
                        </div>

                    </div>
                </asp:View>
        
                <asp:View ID="vwHelp" runat="server">
                    
                    <div id="Help" class="Help" runat="server">
                        
                        <div class="HelpIcon">
                            <asp:Image ID="Image1" ImageUrl="~/images/Calculators/icono_help.png" runat="server"/>
                        </div>

                        <span class="helpText">
                        <p>  
                        This tool calculates loan amounts and mortgage payments for two underwriting scenarios: one that uses aggressive underwriting guidelines and one that uses conservative guidelines.
                        </p>
                        <p>
                        The calculator uses the lower of two ratios for each set of results: payment-to-income ratio (also called housing ratio) and debt-to-income ratio (also called debt ratio).
                        </p>
                        <p>
                        When the economy is strong, lenders are more aggressive and raise these ratios to compete for business. When the economy is weak, lenders are more conservative and lower their ratios.
                        </p>
                        <p>
                        The following housing ratios are used for conservative results: 29% for down payments of less than 20% and 30% for down payments of 20% or more. A debt ratio of 36% is used for all down payments.
                        </p>
                        <p>
                        The following ratios are used for aggressive results: housing and debt ratios of 31% and 38%, respectively, for down payments of less than 10%; housing and debt ratios of 32% and 40%, respectively, for down payments of 10% or more but less than 20%; and housing and debt ratios of 33% and 41%, respectively, for down payments of 20% or more.    
                        </p>
                        </span>
                    </div>
                </asp:View>

            </asp:MultiView>

        </div>

    </div>








