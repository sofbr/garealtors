﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoanCalculator;

namespace GARealtors
{
    public partial class AdjustableRatePayments : System.Web.UI.UserControl
    {

        List<string> errors;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Calculate() {

            LoanTerms terms = new LoanTerms() { 
                LoanAmount=decimal.Parse(txtLoanAmount.Text), 
                AppraisedValue=decimal.Parse(txtAppraisedValue.Text), 
                TermYears=int.Parse(txtTermYears.Text),
                YearlyPropertyInsurance=decimal.Parse(txtPropertyInsurance.Text),
                YearlyPropertyTax = decimal.Parse(txtPropertyTax.Text),
                InterestRate = decimal.Parse(txtInitialInterestRate.Text)                
            };

            terms.DownPayment = ((terms.AppraisedValue - terms.LoanAmount) / terms.AppraisedValue) * 100;
            
            
            FixedRateLoanCalculator calc = new FixedRateLoanCalculator();

            calc.LoanTerms = terms;

            calc.CalculateAdjustable();

            lblPrincipalAndInterst.Text = String.Format("{0:C0}", calc.MonthlyPayment.PrincipalAndInterest);
            lblTaxesAndInsurance.Text = string.Format("{0:C0}", calc.MonthlyPayment.TaxesAndInsurance);
            lblMortgageInsurance.Text = string.Format("{0:C0}", calc.MonthlyPayment.MortgageInsurance);
            lblTotalPayment.Text = string.Format("{0:C0}", calc.MonthlyPayment.TotalPayment);

        }

        protected SchedulePaymentTable GetSchedule() {

            eChangeRate predictedChangeInRate = eChangeRate.InterestRatesWillIncrease;

            if (radInterestSame.Checked) predictedChangeInRate = eChangeRate.InterestRatesWillRemainTheSame;
            if (radInterestIncrease.Checked) predictedChangeInRate = eChangeRate.InterestRatesWillIncrease;
            if (radInterestDecrease.Checked) predictedChangeInRate = eChangeRate.InterestRatesWillDecrease;

            SchedulePaymentTable schedule = new SchedulePaymentTable();
            AdjustableScheduleCalculator calc = new AdjustableScheduleCalculator()
            {
                LoanAmount = decimal.Parse(txtLoanAmount.Text),
                MonthsBeforeFirstAdjustment = int.Parse(txtMonthsBFAdjustment.Text),
                InterestRate = decimal.Parse(txtInitialInterestRate.Text),
                MonthsBetweenRateAdjustments = int.Parse(txtMonthsBRateAdjustments.Text),
                MaximumRateAdjustment = decimal.Parse(txtMaximumRateAdjustment.Text),
                MinimumRate = decimal.Parse(txtMinimumRate.Text),
                MaximumRate = decimal.Parse(txtMaximumRate.Text),
                Margin = decimal.Parse(txtMargin.Text),
                IndexRate = decimal.Parse(txtIndexRate.Text),
                PredictedChangeInRate = predictedChangeInRate,
                MonthsBetweenIndexChange = int.Parse(txtMonthsBIndexAdjustments.Text),
                IndexRateChangePerAdjustment = decimal.Parse(txtIndexChangeRate.Text),
                TermMonths = int.Parse(txtTermYears.Text) * 12,

            };

            schedule.Calculator = calc;

            schedule.BuildSchedulePayment();

            return schedule;

        }

        protected void CalculateSchedulePayments() {

            var schedule = GetSchedule();

            grdSchedulePayment.DataSource = schedule.SchedulePayments;
            grdSchedulePayment.DataBind();

        }

        protected void CalculateChart() {

            var schedule = GetSchedule();

            decimal[] principalPayment = new decimal[5];
            decimal[] interestPayment = new decimal[5];

            foreach (SchedulePayment payment in schedule.SchedulePayments)
            {

                if (payment.Month % 72 == 0)
                {
                    principalPayment[(payment.Month / 72) - 1] = payment.PrincipalPaid;
                    interestPayment[(payment.Month / 72) - 1] = payment.InterestPaid;
                }

            }

            double[] debts = { 5, 10, 15, 20, 30 };

            AdjustableChart.Series[0].Points.DataBindXY(debts, principalPayment);
            AdjustableChart.Series[1].Points.DataBindXY(debts, interestPayment);


            AdjustableChart.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.StackedColumn;
            AdjustableChart.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.StackedColumn;

            AdjustableChart.ChartAreas[0].AxisX.Title = "Monthly payment ($)";
            AdjustableChart.ChartAreas[0].AxisY.Title = "Year";

        }

        protected void ActiveTab(string tab)
        {

            TabInputs.Attributes["class"] = TabInputs.ID == tab ? "cell active" : "cell inactive";
            TabResults.Attributes["class"] = TabResults.ID == tab ? "cell active" : "cell inactive";
            TabGraph.Attributes["class"] = TabGraph.ID == tab ? "cell active" : "cell inactive";
            TabTables.Attributes["class"] = TabTables.ID == tab ? "cell active" : "cell inactive";
            TabHelp.Attributes["class"] = TabHelp.ID == tab ? "cell active" : "cell inactive";

        }
       

        protected void ShowInputs() {
            mvMain.SetActiveView(vwInputs);
            ActiveTab(TabInputs.ID);
        }

        protected void ShowResult() {

            Page.Validate();

            if (!Page.IsValid) return;

            Calculate();

            mvMain.SetActiveView(vwResults);
            ActiveTab(TabResults.ID);
        }

        protected void ShowGraphs()
        {

            Page.Validate();

            if (!Page.IsValid) return;

            mvMain.SetActiveView(vwGraphs);
            ActiveTab(TabGraph.ID);

            CalculateChart();
        }

        protected void ShowSchedule() {

            Page.Validate();

            if (!Page.IsValid) return;

            mvMain.SetActiveView(vwTables);
            ActiveTab(TabTables.ID);

        }

        protected void ShowHelp() {
            mvMain.SetActiveView(vwHelp);
            ActiveTab(TabHelp.ID);
        }

        protected void btnResults_Click(object sender, EventArgs e)
        {           
            ShowResult();
        }

        protected void lnkInputs_Click(object sender, EventArgs e)
        {
            ShowInputs();
        }

        protected void lnkResults_Click(object sender, EventArgs e)
        {           
            ShowResult();
        }

        protected void lnkGraph_Click(object sender, EventArgs e)
        {
            ShowGraphs();
        }

        protected void lnlTable_Click(object sender, EventArgs e)
        {
            CalculateSchedulePayments();
            ShowSchedule();
        }

        protected void lnkHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

          protected void ValidateLoanTerms() { 
                                  
            decimal loanAmount,appraisedValue,interestRate,tax,insurance,
                    federalTax,maximumRateAdjustment,minimumRate,maximumRate,margin,indexRate,indexRateChange;

            int termYears, monthsBFAdjustment, monthsBRateAdjustment, monthsBetweenIndexAdjustments;

            lblLoanAmount.CssClass = "";
            lblAppraisedValue.CssClass = "";
            lblTermYears.CssClass = "";
            lblYearlyPropertyTax.CssClass = "";
            lblYearlyPropertyInsurance.CssClass = "";

            lblStateFederalTax.CssClass = "";
            lblMonthsBeforeFirstAdjustment.CssClass = "";
            lblInitialInterestRate.CssClass = "";
            lblMonthsBetweenRateAdjustments.CssClass = "";
            lblMaximumRateAdjustment.CssClass = "";
            lblMinimumRate.CssClass = "";
            lblMaximumRate.CssClass = "";
            lblMargin.CssClass = "";

            lblIndexRate.CssClass = "";
            lblMonthsBetweenIndexAdjustments.CssClass = "";
            lblIndexChangeRate.CssClass = "";


            if (!Validator.validate(txtLoanAmount.Text, 1m, 1000000000m, out loanAmount)) {
                errors.Add("Loan amount must be a number between 1 and 1,000,000,000.");
                lblLoanAmount.CssClass = "LabelError";
            }
            if (!Validator.validate(txtAppraisedValue.Text, 1m, 1000000000m, out appraisedValue)) {
                errors.Add("Appraised value must be a number between 1 and 1,000,000,000.");
                lblAppraisedValue.CssClass = "LabelError";
            }

            if (!Validator.validate(txtTermYears.Text, 1, 40, out termYears))
            {
                errors.Add("Term (years) must be a whole number between 1 and 40.");
                lblTermYears.CssClass = "LabelError";
            }          
            if (!Validator.validate(txtPropertyTax.Text, 0, 100000, out tax))
            {
                errors.Add("Yearly property tax must be a number between 0 and 100,000.");
                lblYearlyPropertyTax.CssClass = "LabelError";
            }
            if (!Validator.validate(txtPropertyInsurance.Text, 0, 100000, out insurance))
            {
                errors.Add("Yearly property insurance must be a number between 0 and 100,000.");
                lblYearlyPropertyInsurance.CssClass = "LabelError";
            }
            if (!Validator.validate(txtSateFedTaxRate.Text, 0, 90, out federalTax))
            {
                errors.Add("Your state + federal tax rate must be a number between 0.00 and 90.00.");
                lblStateFederalTax.CssClass = "LabelError";
            }
            if (!Validator.validate(txtMonthsBFAdjustment.Text, 1, 120, out monthsBFAdjustment))
            {
                errors.Add("Months before first adjustment must be a whole number between 1 and 120.");
                lblMonthsBeforeFirstAdjustment.CssClass = "LabelError";
            }
            if (!Validator.validate(txtInitialInterestRate.Text, 0, 50, out interestRate))
            {
                errors.Add("Initial interest rate must be a number between 0.000 and 50.000.");
                lblInitialInterestRate.CssClass = "LabelError";
            }
            if (!Validator.validate(txtMonthsBRateAdjustments.Text, 1, 120, out monthsBRateAdjustment))
            {
                errors.Add("Months between rate adjustments must be a whole number between 1 and 120.");
                lblMonthsBetweenRateAdjustments.CssClass = "LabelError";
            }     
            if (!Validator.validate(txtMaximumRateAdjustment.Text, 0m, 20m, out maximumRateAdjustment))
            {
                errors.Add("Maximum rate adjustment must be a number between 0.00 and 20.00.");
                lblMaximumRateAdjustment.CssClass = "LabelError";
            }
            if (!Validator.validate(txtMinimumRate.Text, 0m, 50m, out minimumRate))
            {
                errors.Add("Minimum rate must be a number between 0.00 and 50.00.");
                lblMinimumRate.CssClass = "LabelError";
            }
            if (!Validator.validate(txtMaximumRate.Text, 0m, 50m, out maximumRate))
            {
                errors.Add("Maximum rate must be a number between 0.00 and 50.00.");
                lblMaximumRate.CssClass = "LabelError";
            }
            if (!Validator.validate(txtMargin.Text, -20m, 20m, out margin))
            {
                errors.Add("Margin must be a number between -20.00 and 20.00.");
                lblMargin.CssClass = "LabelError";
            }
            if (!Validator.validate(txtIndexRate.Text, 0m, 50m, out indexRate))
            {
                errors.Add("Index rate must be a number between 0.00 and 50.00.");
                lblIndexRate.CssClass = "LabelError";
            }
            if (!Validator.validate(txtMonthsBIndexAdjustments.Text, 1, 120, out monthsBetweenIndexAdjustments))
            {
                errors.Add("Months between index adjustments must be a whole number between 1 and 120.");
                lblMonthsBetweenIndexAdjustments.CssClass = "LabelError";
            }
            if (!Validator.validate(txtIndexChangeRate.Text, 0m, 20m, out indexRateChange))
            {
                errors.Add("Index rate change per adjustment must be a number between 0.000 and 20.000.");
                lblIndexChangeRate.CssClass = "LabelError";
            }
                   
            
          }

        protected void AdjustableValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            errors = new List<string>();

            ValidateLoanTerms(); 
            
            if(errors.Count>0) args.IsValid = false;

            lstErrores.DataSource = errors;
            lstErrores.DataBind();

        }




    }
}