﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculators.aspx.cs" Inherits="GARealtors.Calculators" %>
<%@ Register Src="~/BorrowQty.ascx" TagPrefix="GARealtors" TagName="BorrowQty" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="Stylesheet" href="css/GARealtors.css" />
    <link rel="Stylesheet" href="css/GARealtorsCalculator.css" />
    <script type="text/javascript" src="Scripts/jquery-1.8.2min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.tools.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        
        <div id="Main">
                               
            <div id="Icon"><img src=" ../images/calculators/icono.png " alt="" /><span>How much can you afford?</span></div>
            <div id="Title"><span>Before you start the search for your new home, it is essential to know how much you can afford. 
            Talk to your REALTOR® about all of the costs associated with the purchase of the home as well as what is included in your mortgage payment, such as private mortgage insurance and taxes. 
            Use the calculators below to get an idea of what you can afford. 
            There are many variables associated with the home purchase, so be advised that the numbers reflected in the calculators below are not final.</span>
            </div>
            <div class="Clear"></div>
                                                   
        </div>

        <div id="Content">
            <div id="calcList1" >
                <ul class="calculatorlist">
                    <li><a rel="#BorrowQtyOverlay" style="text-decoration:none">How much can I borrow?</a></li>
                    <li><a rel="#MortgagePaymentsOverlay" style="text-decoration:none">How much will my mortgage payments be?</a></li>
                    <li><a rel="#AdjustableRatePaymentsOverlay" style="text-decoration:none">How much will my adjustable rate payments be?</a></li>
                    <li>Which is better fixed or adjustable?</li>
                    <li>Should I pay points to lower the rate?</li>
                    <li>Which is better 15- or 30-year loan term? </li>
                    <li>How much should I put down for a new home?</li>
                    <li>How much can I save in taxes?</li>
                    <li>What will my closing costs be?</li>
                    <li>Am I better off renting?</li>
                </ul>
            </div>
            <div id="calcList2" >
                <ul class="calculatorlist">
                    <li>Am I better off refinancing?</li>
                    <li>What will my refinancing costs be?</li>
                    <li>How can I reduce mortgage insurance costs?</li>
                    <li>Which lender has the better loan?</li>
                    <li>Which loan is better?</li>
                    <li>How advantageous are extra payments?</li>
                    <li>What home can I afford?</li>
                    <li>How much will my interest only payment be?</li>
                    <li>Which is better interest-only or traditional?</li>
                    <li>Is my Private Mortgage Insurance deductible?</li>
                </ul>
            </div>
            <div class="Clear"></div>
            
        </div>

        <div class="overlay" id="BorrowQtyOverlay" runat="server">
            <iframe id="frmBorrow" src="BorrowQtyWrapper.aspx" ></iframe>
        </div>

        <div class="overlay" id="MortgagePaymentsOverlay" runat="server">
            <iframe src="MortgagePaymentsWrapper.aspx" ></iframe>
        </div>

        <div class="overlay" id="AdjustableRatePaymentsOverlay" runat="server">
            <iframe src="AdjustableRatePaymentsWrapper.aspx"  ></iframe>
        </div>

                      
    </form>

    <script type="text/javascript">

        $(function () {

            $("a[rel]").overlay({ mask: '#101010', opacity:0.1 });
        });
    
    </script>

</body>
</html>
