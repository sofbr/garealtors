﻿function OnlyWholeNumbers(e) {
    var key;
  
    if (window.event) 
    {
        key = e.keyCode;
    }
    else if (e.which) 
    {
        key = e.which;
    }       

    if ((key >= 48 && key <= 57) || key == 8 || key == 13) {
        return true;
    }
    return false;
}

function OnlyNumbers(e,text) {
    var key;

   
    if (window.event) 
    {
        key = e.keyCode;
    }
    else if (e.which) 
    {
        key = e.which;
    }
 
    if ((key >= 48 && key <= 57) || key == 8 || key == 13) {
        return true;
    }

    if (key == 46) {
        if (text.value.indexOf(".") == -1) return true;
        else return false;
    }

    return false;
}