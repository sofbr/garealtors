﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdjustableRatePayments.ascx.cs" Inherits="GARealtors.AdjustableRatePayments" %>

 <div id="CalculatorWrapper">

    <div id="CalculatorTitle">
            <span>How much will my adjustable rate payments be?</span>
     </div>

    <div class="CalculatorContent">

    <section id="Tabs" class="table">
        <div id="TabInputs" class="cell active" runat="server">
            <asp:LinkButton ID="lnkInputs" Text="Inputs" runat="server" 
                onclick="lnkInputs_Click" />
        </div>        
        <div id="TabResults" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnkResults" Text="Results" runat="server" 
                onclick="lnkResults_Click"  />
        </div>
        <div  id="TabGraph" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnkGraph" Text="Graph" runat="server" 
                onclick="lnkGraph_Click"  />
        </div>
        <div id="TabTables" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnlTable" Text="Tables" runat="server" 
                onclick="lnlTable_Click" />
        </div>
        <div id="TabHelp" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnkHelp" Text="Help" runat="server" 
                onclick="lnkHelp_Click"  />
        </div>
    </section>

    <asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">

        <asp:View ID="vwInputs" runat="server">       
            <div id="Inputs" class="Inputs" runat="server">

                <div>
                    <asp:ValidationSummary ID="valSummary" EnableClientScript="true" ShowMessageBox="false" DisplayMode="List" ShowSummary="true" runat="server" />
                    <asp:CustomValidator runat="server" ID="AdjustableValidator"  onservervalidate="AdjustableValidator_ServerValidate" />
                </div>

                <asp:ListView ID="lstErrores" runat="server">
                    <LayoutTemplate>
                        <div class="isa_error">
                            <span>Please fix following errors</span>
                            <ul>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </ul>
                        </div>
                    </LayoutTemplate>           
                    <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>                    
                </asp:ListView>

                <section class="table InputFields">
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblLoanAmount" Text="Loan Amount" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtLoanAmount" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblAppraisedValue" Text="Appraised value" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtAppraisedValue" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblTermYears" Text="Term (years)" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtTermYears" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblYearlyPropertyInsurance" Text="Yearly property insurance" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtPropertyInsurance" runat="server"/></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblYearlyPropertyTax" Text="Yearly property tax" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtPropertyTax" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblStateFederalTax" Text="Your state + federal tax rate" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtSateFedTaxRate" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMonthsBeforeFirstAdjustment" Text="Months before first adjustment" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMonthsBFAdjustment" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblInitialInterestRate" Text="Initial interest rate" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtInitialInterestRate" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMonthsBetweenRateAdjustments" Text="Months between rate adjustments" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMonthsBRateAdjustments" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMaximumRateAdjustment" Text="Maximum rate adjustment" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMaximumRateAdjustment" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMinimumRate" Text="Minimum rate" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMinimumRate" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMaximumRate" Text="Maximum rate" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMaximumRate"  runat="server"/></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMargin" Text="Margin" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMargin" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblIndexRate" Text="Index rate" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtIndexRate" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblMonthsBetweenIndexAdjustments" Text="Months between index adjustments" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtMonthsBIndexAdjustments" runat="server" /></div>
                    </div>    
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblIndexChangeRate" Text="Index rate change per adjustment" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtIndexChangeRate" runat="server" /></div>
                    </div>    
                </section>

                <section class="table InputFields">
                    <div class="header">
                        <div class="cell col1">Predicted change in rate</div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:RadioButton ID="radInterestSame" GroupName="Predicted" Text="Interest rate will remain the same" TextAlign="Right" runat="server"/></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:RadioButton ID="radInterestIncrease" Checked="true" GroupName="Predicted" Text="Interest rate will increase" TextAlign="Right" runat="server"/></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:RadioButton ID="radInterestDecrease" GroupName="Predicted" Text="Interest rate will decrease" TextAlign="Right" runat="server" /></div>
                    </div>       
                </section>
   

                <asp:Button ID="btnResults" Text="Results..." CssClass="Button" runat="server" onclick="btnResults_Click" />

            </div>
        </asp:View>

        <asp:View ID="vwResults" runat="server">
            <div id="Results" runat="server">
                
                <div id="ResultsTitle">Your initial monthly payment</div>

                <table id="monthlyPayment">
                    <tbody>
                        <tr>
                            <td>Principal and interest</td>
                            <td><asp:Label ID="lblPrincipalAndInterst" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Taxes and insurance</td>
                            <td><asp:Label ID="lblTaxesAndInsurance" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Mortgage insurance</td>
                            <td><asp:Label ID="lblMortgageInsurance" runat="server" /></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total payment</td>
                            <td><asp:Label ID="lblTotalPayment" runat="server" /></td>
                        </tr>
                    </tfoot>
                </table>    
            </div>        
        </asp:View>

        <asp:View ID="vwGraphs" runat="server">
            <div id="Graphs" class="Chart" runat="server">

                <div id="GraphTitle">Your payments over time</div>

                <div id="TheGraph">
        
                    <asp:Chart ID="AdjustableChart" runat="server" Width="550px">
                        <Series>
                            <asp:Series Name="Principal" Legend="Legend1">
                            </asp:Series>
                            <asp:Series Name="Interest" Legend="Legend1">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Name="Legend1">
                            </asp:Legend>
                        </Legends>
                    </asp:Chart>
    
                </div>

                <div id="GraphFooter">        
    
                </div>

            </div>        
        </asp:View>

        <asp:View ID="vwTables" runat="server">
            <div id="Tables" class="Tables" runat="server">
    
                <div id="TableTitle">Your monthly payment schedule</div>

                 <asp:GridView ID="grdSchedulePayment" AutoGenerateColumns="false" CssClass="TableSchedule" runat="server">
                    <Columns>
                        <asp:BoundField DataField="Month" HeaderText="Month"  />
                        <asp:BoundField DataField="MonthlyPayment" DataFormatString="{0:C0}" HeaderText="Monthly Payment" />
                        <asp:BoundField DataField="Balance" DataFormatString="{0:C0}" HeaderText="Balance" />
                        <asp:BoundField DataField="PrincipalPaid" DataFormatString="{0:C0}" HeaderText="Principal Paid" />
                        <asp:BoundField DataField="InterestPaid" DataFormatString="{0:C0}" HeaderText="Interest Paid" />
                        <asp:BoundField DataField="CumulativeInterest" DataFormatString="{0:C0}" HeaderText="Cumulative Interest" />
                    </Columns>
                </asp:GridView>

                <div id="pager"></div>

            </div>        
        </asp:View>

        <asp:View ID="vwHelp" runat="server">
            <div id="Help" class="Help" runat="server">
                <div class="HelpIcon">
                    <asp:Image ID="Image1" ImageUrl="~/images/Calculators/icono_help.png" runat="server"/>
                </div>
                <span class="helpText">
                    <p>
                    This tool calculates your monthly payment for an adjustable-rate mortgage (ARM) loan, given a loan amount and loan terms. Payments on an adjustable-rate mortgage are fixed for an initial period and are usually adjusted annually after the initial period. For example, a 3/1 ARM loan would have a fixed rate for the first three years and be readjusted once a year thereafter.
                    </p>
                    <p>
                    The interest rate on an adjustable-rate mortgage loan is usually reset on the loan's anniversary date. To calculate the new rate, a spread, or margin, is added to a widely used index rate. 
                    </p>
                    <p>
                    Adjustable-rate mortgage loans usually have a periodic and lifetime cap that limit how much the interest rate can change in one period and the maximum interest rate during the lifetime of the loan, respectively.
                    </p>        
                </span>
            </div>        
        </asp:View>

    </asp:MultiView>

    </div>

</div>










