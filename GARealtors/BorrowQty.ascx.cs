﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoanCalculator;
using System.Drawing;


namespace GARealtors
{
    public partial class BorrowQty : System.Web.UI.UserControl
    {

        List<string> errors;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        EstimateCalculator BuildCalc() {
            LoanTerms loanTerms = new LoanTerms();
            MonthlyIncome income = new MonthlyIncome();
            Debts payments = new Debts();
            EstimateCalculator calc;

            income.WageIncome = decimal.Parse(txtWages_Income.Text);
            income.InvestmentIncome = decimal.Parse(txtInvestment_Income.Text);
            income.IncomeFromRental = decimal.Parse(txtRental_Income.Text);
            income.OtherIncome = decimal.Parse(txtOther_Income.Text);

            payments.AutoLoans = decimal.Parse(txtAutoLoans_Payment.Text);
            payments.StudentLoans = decimal.Parse(txtStudentLoans_Payments.Text);
            payments.OtherPayments = decimal.Parse(txtOther_Payment.Text);

            payments.MonthlyAlimony = decimal.Parse(txtAlimony_OtherDebts.Text);
            payments.MonthlyCreditCardPayments = decimal.Parse(txtMonthlyCreditCard_OtherDebts.Text);

            loanTerms.InterestRate = decimal.Parse(txtInterestRate.Text);
            loanTerms.TermYears = int.Parse(txtTerms.Text);
            loanTerms.DownPayment = decimal.Parse(txtDownPayment.Text);

            loanTerms.YearlyPropertyInsurance = decimal.Parse(txtYearlyPropertyTax.Text);
            loanTerms.YearlyPropertyTax = decimal.Parse(txtYearlyPropertyInsurance.Text);

            calc = new EstimateCalculator(eEstimateType.Conservative)
            {
                loanTerms = loanTerms,
                monthlyIncome = income,
                MonthlyPayments = payments
            };

            return calc;
        }
        
        void Calculate() {

            var calc = BuildCalc();

            lblCCustomPercent.Text = string.Format("{0:P}", calc.loanTerms.DownPayment/100);
            lblACustomPercent.Text = string.Format("{0:P}", calc.loanTerms.DownPayment/100);

            calc.CalculatePayments(10);
            Store10PercentResults(calc.LoanPayments);
            calc.CalculatePayments(20);
            Store20PercentResults(calc.LoanPayments);
            calc.CalculatePayments(30);
            Store30PercentResults(calc.LoanPayments);
            calc.CalculatePayments(decimal.Parse(txtDownPayment.Text));
            StoreCustomPercentResults(calc.LoanPayments);

            calc.EstimateType = eEstimateType.Aggresive;

            calc.CalculatePayments(10);
            StoreAggresive10PercentResult(calc.LoanPayments);
            calc.CalculatePayments(20);
            StoreAggresive20PercentResult(calc.LoanPayments);
            calc.CalculatePayments(30);
            StoreAggresive30PercentResult(calc.LoanPayments);
            calc.CalculatePayments(decimal.Parse(txtDownPayment.Text));
            StoreAggresiveCustomPercentResults(calc.LoanPayments);
            
   
        }

        MonthlyPayment CalculateGraph() {

            var calc = BuildCalc();

            calc.CalculatePayments(35);

            return calc.LoanPayments;

        }

        protected void Store10PercentResults(MonthlyPayment payments) {

            lblCDownPayment10.Text = string.Format("{0:C0}", payments.DownPaymentAmount);
            lblCLoanAmount10.Text =  string.Format("{0:C0}",payments.LoanAmount);
            lblCPriceHome10.Text = string.Format("{0:C0}",payments.PriceOfHome);

            lblCPrincipalPayment10.Text = string.Format("{0:C0}",payments.PrincipalAndInterest);
            lblCTaxesAndIns10.Text = string.Format("{0:C0}",payments.TaxesAndInsurance);
            lblCMortgage10.Text = string.Format("{0:C0}",payments.MortgageInsurance);
            lblCTotalPayment10.Text = string.Format("{0:C0}", payments.TotalPayment);
        }

        protected void Store20PercentResults(MonthlyPayment payments){
            lblCDownPayment20.Text = string.Format("{0:C0}",payments.DownPaymentAmount);
            lblCLoanAmount20.Text = string.Format("{0:C0}",payments.LoanAmount);
            lblCPriceHome20.Text = string.Format("{0:C0}", payments.PriceOfHome);

            lblCPrincipalPayment20.Text = string.Format("{0:C0}",payments.PrincipalAndInterest);
            lblCTaxesAndIns20.Text = string.Format("{0:C0}",payments.TaxesAndInsurance);
            lblCMortgage20.Text = string.Format("{0:C0}",payments.MortgageInsurance);
            lblCTotalPayment20.Text = string.Format("{0:C0}", payments.TotalPayment);
        }

        protected void Store30PercentResults(MonthlyPayment payments)
        {
            lblCDownPayment30.Text = string.Format("{0:C0}",payments.DownPaymentAmount);
            lblCLoanAmount30.Text = string.Format("{0:C0}",payments.LoanAmount);
            lblCPriceHome30.Text = string.Format("{0:C0}", payments.PriceOfHome);

            lblCPrincipalPayment30.Text = string.Format("{0:C0}",payments.PrincipalAndInterest);
            lblCTaxesAndIns30.Text = string.Format("{0:C0}",payments.TaxesAndInsurance);
            lblCMortgage30.Text = string.Format("{0:C0}",payments.MortgageInsurance);
            lblCTotalPayment30.Text = string.Format("{0:C0}",payments.TotalPayment);
        }

        protected void StoreCustomPercentResults(MonthlyPayment payments)
        {            
            lblCDownPaymentCustom.Text = string.Format("{0:C0}",payments.DownPaymentAmount);
            lblCLoanAmountCustom.Text = string.Format("{0:C0}",payments.LoanAmount);
            lblCPriceHomeCustom.Text = string.Format("{0:C0}",payments.PriceOfHome);

            lblCPrincipalPaymentCustom.Text = string.Format("{0:C0}",payments.PrincipalAndInterest);
            lblCTaxesAndInsCustom.Text = string.Format("{0:C0}",payments.TaxesAndInsurance);
            lblCMortgageCustom.Text = string.Format("{0:C0}",payments.MortgageInsurance);
            lblCTotalPaymentCustom.Text = string.Format("{0:C0}",payments.TotalPayment);
        }

        protected void StoreAggresive10PercentResult(MonthlyPayment payments) {
            lblADownPayment10.Text = string.Format("{0:C0}", payments.DownPaymentAmount);
            lblALoanAmount10.Text = string.Format("{0:C0}", payments.LoanAmount);
            lblAPriceHome10.Text = string.Format("{0:C0}", payments.PriceOfHome);

            lblAPrincipalPayment10.Text = string.Format("{0:C0}", payments.PrincipalAndInterest);
            lblATaxesAndIns10.Text = string.Format("{0:C0}", payments.TaxesAndInsurance);
            lblAMortgage10.Text = string.Format("{0:C0}", payments.MortgageInsurance);
            lblATotalPayment10.Text = string.Format("{0:C0}", payments.TotalPayment);
        }

        protected void StoreAggresive20PercentResult(MonthlyPayment payments) {
            lblADownPayment20.Text = string.Format("{0:C0}", payments.DownPaymentAmount);
            lblALoanAmount20.Text = string.Format("{0:C0}", payments.LoanAmount);
            lblAPriceHome20.Text = string.Format("{0:C0}", payments.PriceOfHome);

            lblAPrincipalPayment20.Text = string.Format("{0:C0}", payments.PrincipalAndInterest);
            lblATaxesAndIns20.Text = string.Format("{0:C0}", payments.TaxesAndInsurance);
            lblAMortgage20.Text = string.Format("{0:C0}", payments.MortgageInsurance);
            lblATotalPayment20.Text = string.Format("{0:C0}", payments.TotalPayment);
        }

        protected void StoreAggresive30PercentResult(MonthlyPayment payments) {
            lblADownPayment30.Text = string.Format("{0:C0}", payments.DownPaymentAmount);
            lblALoanAmount30.Text = string.Format("{0:C0}", payments.LoanAmount);
            lblAPriceHome30.Text = string.Format("{0:C0}", payments.PriceOfHome);

            lblAPrincipalPayment30.Text = string.Format("{0:C0}", payments.PrincipalAndInterest);
            lblATaxesAndIns30.Text = string.Format("{0:C0}", payments.TaxesAndInsurance);
            lblAMortgage30.Text = string.Format("{0:C0}", payments.MortgageInsurance);
            lblATotalPayment30.Text = string.Format("{0:C0}", payments.TotalPayment);
        }

        protected void StoreAggresiveCustomPercentResults(MonthlyPayment payments) {
            lblADownPaymentCustom.Text = string.Format("{0:C0}", payments.DownPaymentAmount);
            lblALoanAmountCustom.Text = string.Format("{0:C0}", payments.LoanAmount);
            lblAPriceHomeCustom.Text = string.Format("{0:C0}", payments.PriceOfHome);

            lblAPrincipalPaymentCustom.Text = string.Format("{0:C0}", payments.PrincipalAndInterest);
            lblATaxesAndInsCustom.Text = string.Format("{0:C0}", payments.TaxesAndInsurance);
            lblAMortgageCustom.Text = string.Format("{0:C0}", payments.MortgageInsurance);
            lblATotalPaymentCustom.Text = string.Format("{0:C0}", payments.TotalPayment);            
        }

        protected void ActiveTab(string tab) { 
            
            TabInputs.Attributes["class"]=TabInputs.ID==tab?"cell active":"cell inactive";
            TabResults.Attributes["class"] = TabResults.ID == tab ? "cell active" : "cell inactive";
            TabGraph.Attributes["class"] = TabGraph.ID == tab ? "cell active" : "cell inactive";
            TabHelp.Attributes["class"] = TabHelp.ID == tab ? "cell active" : "cell inactive";

        }

        protected void ShowInputs(){          
            mvMain.SetActiveView(vwInputs);
            ActiveTab(TabInputs.ID);
        }

        protected void ShowResults() {

            Page.Validate();
            if (!Page.IsValid) return;
                 
            Calculate();
          
            mvMain.SetActiveView(vwResults);                        
            ActiveTab(TabResults.ID);
        }

        protected void ShowGraph(){

            Page.Validate();
            if (!Page.IsValid) return;
        

            decimal totalIncome = decimal.Parse(txtWages_Income.Text);
            totalIncome += decimal.Parse(txtInvestment_Income.Text);
            totalIncome += decimal.Parse(txtOther_Income.Text);

            var payments = CalculateGraph();


            decimal[] loanAmount = new decimal[5] { 
                Calculator.GetMonthlyPayment(totalIncome,0,35,eEstimateType.Conservative),
                Calculator.GetMonthlyPayment(totalIncome,400,35,eEstimateType.Conservative),
                Calculator.GetMonthlyPayment(totalIncome,800,35,eEstimateType.Conservative),
                Calculator.GetMonthlyPayment(totalIncome,1200,35,eEstimateType.Conservative),
                Calculator.GetMonthlyPayment(totalIncome,1600,35,eEstimateType.Conservative),

            };

            double[] debts = { 0, 400, 800, 1200, 1600 };

            BorrowChart.Series[0].Points.DataBindXY(debts, loanAmount);
            BorrowChart.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;

            BorrowChart.ChartAreas[0].AxisX.Title = "Monthly debt ($)";
            BorrowChart.ChartAreas[0].AxisY.Title = "Loan mount ($)";

            mvMain.SetActiveView(vwGraph);
            ActiveTab(TabGraph.ID);
        }

        protected void ShowHelp(){          
            mvMain.SetActiveView(vwHelp);
            ActiveTab(TabHelp.ID);
        }
       

        protected void btnResults_Click(object sender, EventArgs e)
        {
            ShowResults();
        }

        protected void lnkInputs_Click(object sender, EventArgs e)
        {
            ShowInputs();
        }

        protected void lnlResults_Click(object sender, EventArgs e)
        {
            ShowResults();
        }

        protected void lnkGraph_Click(object sender, EventArgs e)
        {
            ShowGraph();          

           

        }

        protected void lnkHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }
      

        protected void ValidateIncome() {           
         
            decimal wage,investment,otherIncome, rental;
           

            decimal totalIncome;

            lblWages.CssClass = "";
            lblInvestment.CssClass = "";
            lblRental.CssClass = "";
            lblOtherIncome.CssClass = "";

            if (!Validator.validate(txtWages_Income.Text, 0m , 100000m,out wage)) {
                errors.Add("Wages before taxes or deductions must be a number between 0 and 100,000.");
                lblWages.CssClass = "LabelError";
            }

            if (!Validator.validate(txtInvestment_Income.Text,0m ,10000000m,out investment)) {
                errors.Add("Investment income before taxes must be a number between 0 and 10,000,000.");
                lblInvestment.CssClass = "LabelError";            
            }

            if (!Validator.validate(txtRental_Income.Text,0m , 100000m,out rental))
            {
                errors.Add("Income from rental properties must be a number between 0 and 100,000.");
                lblRental.CssClass = "LabelError";                
            }

            if (!Validator.validate(txtOther_Income.Text,0m,100000m,out otherIncome))
            {
                errors.Add("Other income must be a number between 0 and 100,000.");
                lblOtherIncome.CssClass = "LabelError";             
            }

            totalIncome = wage + investment + otherIncome + rental;

            if (totalIncome <= 0)
            {
                errors.Add("Gross Income too small, must be greater than zero.");          
            }
           

        }

      

        protected void ValidateDebts() {

            decimal autoLoans, studentLoans, rentalPayment, OtherPayment,alimony,creditCard;

         

            lblAutoLoans.CssClass = "";
            lblStudentLoans.CssClass = "";
            lblRentalPayment.CssClass = "";
            lblOtherPayment.CssClass = "";

            lblAlimony.CssClass = "";
            lblCreditCard.CssClass = "";

            if (!Validator.validate(txtAutoLoans_Payment.Text,  0 ,10000,out autoLoans))
            {
                errors.Add("Auto loans must be a number between 0 and 10,000.");
                lblAutoLoans.CssClass = "LabelError";             
            }

            if (!Validator.validate(txtStudentLoans_Payments.Text, 0 , 10000,out studentLoans))
            {
                errors.Add("Student loans must be a number between 0 and 10,000.");
                lblStudentLoans.CssClass = "LabelError";
            }

            if (!Validator.validate(txtRental_Payment.Text,0 , 10000 , out rentalPayment))
            {
                errors.Add("Rental property loans ($0 if refinancing) must be a number between 0 and 10,000.");
                lblRentalPayment.CssClass = "LabelError";               
            }

            if (!Validator.validate(txtOther_Payment.Text, 0 , 10000, out OtherPayment))
            {
                errors.Add("Other payments must be a number between 0 and 10,000.");
                lblOtherPayment.CssClass = "LabelError";              
            }

            if (!Validator.validate(txtAlimony_OtherDebts.Text, 0 , 10000,out alimony))
            {
                errors.Add("Monthly alimony, child support or other must be a number between 0 and 10,000.");
                lblAlimony.CssClass = "LabelError";             
            }

            if (!Validator.validate(txtMonthlyCreditCard_OtherDebts.Text, 0 , 10000,out creditCard))
            {
                errors.Add("Monthly credit card payments must be a number between 0 and 10,000.");
                lblCreditCard.CssClass = "LabelError";
            }
          
        }

        protected void validateLoanTerms() {           

            lblInterestRate.CssClass = "";
            lblTermYears.CssClass = "";
            lblDownPayment.CssClass = "";

            decimal interestRate,downPayment;
            int termYears;
                                 

            if (!Validator.validate(txtInterestRate.Text, 0 , 50, out interestRate)) {
                errors.Add("Interest rate must be a number between 0.000 and 50.000.");
                lblInterestRate.CssClass = "LabelError";
            }

            if (!Validator.validate(txtTerms.Text , 1 , 40 ,out termYears))
            {
                errors.Add("Term (years) must be a whole number between 1 and 40.");
                lblTermYears.CssClass = "LabelError";                                             
            }
          

            if (!Validator.validate(txtDownPayment.Text ,  0 , 99, out downPayment))
            {                
                errors.Add("Down payment (% of price) must be a number between 0.00 and 99.00.");
                lblDownPayment.CssClass = "LabelError";
            }

        

        }

        protected void validatePropertyTax() { 
        
            decimal tax; 
            decimal insurance; 

            lblYearlyTax.CssClass = "";
            lblYearlyInsurance.CssClass = "";

            if (!Validator.validate(txtYearlyPropertyTax.Text, 0m ,100000m, out tax)) {
                errors.Add("Yearly property tax must be a number between 0 and 100,000.");
                lblYearlyTax.CssClass = "LabelError";
            }

            if (!Validator.validate(txtYearlyPropertyInsurance.Text, 0m , 100000m, out insurance))
            {
                errors.Add("Yearly property insurance must be a number between 0 and 100,000.");
                lblYearlyInsurance.CssClass = "LabelError";
            }            
            
        }

        protected void BorrowValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            errors = new List<string>();

            ValidateIncome(); 
            ValidateDebts(); 
            validateLoanTerms();
            validatePropertyTax();

            if (errors.Count > 0) args.IsValid = false;

            lstErrores.DataSource = errors;
            lstErrores.DataBind();
        }


    }
}