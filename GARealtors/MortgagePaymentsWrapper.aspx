﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MortgagePaymentsWrapper.aspx.cs" Inherits="GARealtors.MortgagePaymentsWrapper" %>
<%@ Register Src="~/MortgagePayments.ascx" TagPrefix="GARealtors" TagName="MortgagePayments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <link href="css/GaRealtors.css" rel="Stylesheet" type="text/css" />
    <link href="css/GaRealtorsCalculator.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <GARealtors:MortgagePayments ID="mortgagePayments" runat="server" />
    </div>
    </form>
</body>
</html>
