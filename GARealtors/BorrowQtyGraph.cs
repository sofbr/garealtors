﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GARealtors
{
    public class BorrowQtyGraph
    {

        protected string _script = @"<script type=""text/javascript"">
      
        

    function RenderGraph(){{

            var example = 'column-basic', theme = 'default';
          
       
            $('#TheGraph').highcharts({{
                title: {{
                    text: 'Monthly Average Temperature',
                    x: -20 
                }},
                subtitle: {{
                    text: 'Source:',
                    x: -20
                }},
                xAxis: {{
                    categories: ['{0}']
                }},
                yAxis: {{
                    title: {{
                        text: 'Loan amount($)'
                    }},
                    plotLines: [{{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }}]
                }},
                tooltip: {{
                    valueSuffix: '°C'
                }},
                legend: {{
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                }},
                series: [{{
                    name: '{1}',
                    data: [{2}]
                }}]
            }});

        }}        
  
   	
</script>";

        public string[] data { get; set; }
        public string[] categories {get;set;}
        public string seriesTitle{ get;set;}


        public string Script { get { return string.Format(_script, string.Join("', '",categories),seriesTitle, string.Join(", ",data)); } }

    }
}