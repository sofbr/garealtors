﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MortgagePayments.ascx.cs" Inherits="GARealtors.MortgagePayments" %>

    <div id="CalculatorWrapper">

    <div id="CalculatorTitle">
            <span>How much will my mortgage payments be?</span>
     </div>

    <div class="CalculatorContent">

    <section id="Tabs" class="table">
        <div id="TabInputs" class="cell active" runat="server">
            <asp:LinkButton ID="lnkInputs" Text="Inputs" runat="server" onclick="lnkInputs_Click" />
        </div>        
        <div id="TabResults" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnkResults" Text="Results" runat="server" onclick="lnkResults_Click"  />
        </div>
        <div id="TabGraph" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnkGraph" Text="Graph" runat="server" onclick="lnkGraph_Click"  />
        </div>
        <div id="TabTables" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnlTable" Text="Tables" runat="server" onclick="lnlTable_Click" />
        </div>
        <div id="TabHelp" class="cell inactive" runat="server">
            <asp:LinkButton ID="lnkHelp" Text="Help" runat="server" onclick="lnkHelp_Click"  />
        </div>
    </section>

    <asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">
        
        <asp:View ID="vwInputs" runat="server">
            <div id="Inputs" class="Inputs" runat="server">

                <div>
                    <asp:ValidationSummary ID="valSummary" EnableClientScript="true" ShowMessageBox="false" DisplayMode="List" ShowSummary="true" runat="server" />
                    <asp:CustomValidator runat="server" ID="MortgageValidator"  onservervalidate="BorrowValidator_ServerValidate" />
                </div>

                <asp:ListView ID="lstErrores" runat="server">
                    <LayoutTemplate>
                        <div class="isa_error">
                            <span>Please fix following errors</span>
                            <ul>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </ul>
                        </div>
                    </LayoutTemplate>           
                    <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>                    
                </asp:ListView>

                <section class="table InputFields">
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblLoanAmount" Text="Loan amount" runat="server"></asp:Label></div>
                        <div class="cell col2"><asp:TextBox ID="txtLoanAmount" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblAppraisedValue" Text="Appraised value" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtAppraisedValue" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblTermYears" Text="Term (years)" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtTermYears"  runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblInterestRate" Text="Interest rate" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtInterestRate" runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblPropertyTax" Text="Yearly property tax" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtPropertyTax"  runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="cell col1"><asp:Label ID="lblPropertyInsurance" Text="Yearly property insurance" runat="server" /></div>
                        <div class="cell col2"><asp:TextBox ID="txtPropertyInsurance"  runat="server" /></div>
                    </div>
                </section>
  

                <asp:Button ID="btnResults" CssClass="Button" Text="Results..." runat="server" onclick="btnResults_Click"/>
            </div>
        </asp:View>

        <asp:View ID="vwResults" runat="server">        
            <div id="Result" runat="server">
    
                <table id="monthlyPayment">
                    <thead>        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Principal and interest</td>
                            <td><asp:Label ID="lblPrincipalAndInterest"  runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Taxes and insurance</td>
                            <td><asp:Label ID="lblTaxesAndInsurance" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Mortgage insurance</td>
                            <td><asp:Label id="lblMortgageInsurance" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Total payment</td>
                            <td><asp:Label ID="lblTotalPayment" runat="server" /></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </asp:View>

        <asp:View ID="vwGraphs" runat="server">
            <div id="Graphs" runat="server">
                
                <div id="GraphTitle">Your payments over the years</div>

                <div id="TheGraph">
        
                    <asp:Chart ID="MortgageChart" Width="550px" runat="server">
                        <Series>
                            <asp:Series Name="Principal" Legend="SeriesL" LegendText="Principal">
                            </asp:Series>
                            <asp:Series Name="Interest" Legend="SeriesL" LegendText="Interest">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Name="SeriesL">
                            </asp:Legend>
                        </Legends>
                    </asp:Chart>
    
                </div>

                <div id="GraphFooter">
                    <p>Although your monthly payment is fixed, the amounts applied to Principal and interest change as you make your payments. Initially, your payment is applied almost entirely to the interest you owe. The longer you pay on the loan, the greater the amount that is applied toward the Principal; the amount that you have borrowed and must repay.</p>
                </div>

            </div>        
        </asp:View>

        <asp:View ID="vwTables" runat="server">
            <div id="PaymentSchedule" class="Tables" runat="server">
      
                <div id="TableTitle">Your annual payment schedule</div>
   
                <asp:GridView ID="grdSchedulePayment" AutoGenerateColumns="false" CssClass="TableSchedule" runat="server">
                    <Columns>
                        <asp:BoundField DataField="Month" HeaderText="Month"  />
                        <asp:BoundField DataField="MonthlyPayment" DataFormatString="{0:C0}" HeaderText="Monthly Payment" />
                        <asp:BoundField DataField="Balance" DataFormatString="{0:C0}" HeaderText="Balance" />
                        <asp:BoundField DataField="PrincipalPaid" DataFormatString="{0:C0}" HeaderText="Principal Paid" />
                        <asp:BoundField DataField="InterestPaid" DataFormatString="{0:C0}" HeaderText="Interest Paid" />
                        <asp:BoundField DataField="CumulativeInterest" DataFormatString="{0:C0}" HeaderText="Cumulative Interest" />
                    </Columns>
                </asp:GridView>   

            </div>
        </asp:View>    
            
        <asp:View ID="vwHelp" runat="server">
            <div id="Help" class="Help" runat="server">
                <div class="HelpIcon">
                    <asp:Image ID="Image1" ImageUrl="~/images/Calculators/icono_help.png" runat="server"/>
                </div>
                <span class="helpText">
                This tool calculates your monthly mortgage payment for a given purchase price, down payment, interest rate, and loan term.

                If the loan amount is more than 80% of the home's purchase price, the calculator estimates a monthly amount for private mortgage insurance (PMI). Mortgage lenders generally require that you obtain mortgage insurance if your down payment is less than 20% of the home purchase price. 
                </span>
            </div>
        </asp:View>

    </asp:MultiView>

    </div>

    </div>










