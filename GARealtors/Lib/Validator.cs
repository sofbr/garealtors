﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GARealtors
{
    public class Validator
    {

        public static bool validate(string sValue, decimal minimum, decimal maximum,out decimal value,decimal? defaultValue=null) {

            bool parsed = decimal.TryParse(sValue, out value);

            if (!parsed && defaultValue.HasValue) value = defaultValue.Value;
            else if(!parsed) return false;
            
            if (value < minimum || value > maximum) return false;

            return true;
        }

        public static bool validate(string sValue, int minimum, int maximum,out int value, int? defaultValue = null)
        {
            bool parsed = int.TryParse(sValue, out value);

            if (!parsed && defaultValue.HasValue) value = defaultValue.Value;
            else if(!parsed) return false;

            if (value < minimum || value > maximum) return false;

            return true;
        }

    }
}