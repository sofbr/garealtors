﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class MonthlyIncome
    {

        public decimal WageIncome { get; set; }
        public decimal InvestmentIncome { get; set; }
        public decimal IncomeFromRental { get; set; }
        public decimal OtherIncome { get; set; }

        public decimal TotalIncome { get { return WageIncome + InvestmentIncome + IncomeFromRental + OtherIncome; } }
     

    }
}
