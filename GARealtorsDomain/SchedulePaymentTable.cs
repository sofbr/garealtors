﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class SchedulePayment
    {

        public int Month { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal Balance { get; set; }
        public decimal PrincipalPaid { get; set; }
        public decimal InterestPaid { get; set; }
        public decimal CumulativeInterest { get; set; }
        public decimal TaxesSaved { get; set; }

    }

    public abstract class ISchedulePaymentCalculator {

        public decimal LoanAmount { get; set; }
        public decimal InterestRate { get; set; }
        public int TermMonths { get; set; }

        public abstract List<SchedulePayment> Calculate();

    }

    public class FixedSchedulePaymentCalculator : ISchedulePaymentCalculator {        

        public override List<SchedulePayment> Calculate()
        {
            List<SchedulePayment> SchedulePayments = new List<SchedulePayment>();
            SchedulePayment payment;
            decimal monthlyPayment = Calculator.GetA(LoanAmount, InterestRate, TermMonths);
            decimal monthlyInterestRate = InterestRate / 12 / 100;

            for (int i = 0; i < TermMonths; i++)
            {

                payment = new SchedulePayment()
                {
                    Month = i + 1,
                    MonthlyPayment = monthlyPayment,
                    InterestPaid = i == 0 ? LoanAmount * monthlyInterestRate : SchedulePayments[i - 1].Balance * monthlyInterestRate,
                };

                payment.CumulativeInterest = i == 0 ? payment.InterestPaid : SchedulePayments[i - 1].CumulativeInterest + payment.InterestPaid;
                payment.PrincipalPaid = monthlyPayment - payment.InterestPaid;
                payment.Balance = i == 0 ? LoanAmount - payment.PrincipalPaid : SchedulePayments[i - 1].Balance - payment.PrincipalPaid;

                SchedulePayments.Add(payment);
            }

            return SchedulePayments;

        }

    }

    public enum eChangeRate { InterestRatesWillRemainTheSame, InterestRatesWillIncrease,InterestRatesWillDecrease }

    public class AdjustableScheduleCalculator : ISchedulePaymentCalculator {

        public int MonthsBeforeFirstAdjustment { get; set; }
        public int MonthsBetweenRateAdjustments { get; set; }

        protected decimal _Balance;
        protected decimal _monthlyPayment;
        protected decimal _monthlyInterestRate;

        public decimal MaximumRateAdjustment { get; set; }
        public decimal MinimumRate { get; set; }
        public decimal MaximumRate { get; set; }
        public decimal Margin { get; set; }
        public decimal IndexRate { get; set; }
        public int MonthsBetweenIndexChange { get; set; }
        public decimal IndexRateChangePerAdjustment { get; set; }

        public eChangeRate PredictedChangeInRate { get; set; }

        public void AdjustIndexRate(int currentMonth) {

            if (currentMonth == 1) return;
            if ((currentMonth-1) % MonthsBetweenIndexChange == 0) IndexRate += IndexRateChangePerAdjustment;

        }

        public void AdjustInterestRate(int currentMonth) {

            if (PredictedChangeInRate == eChangeRate.InterestRatesWillRemainTheSame || PredictedChangeInRate==eChangeRate.InterestRatesWillDecrease) return;

            if (currentMonth <= MonthsBeforeFirstAdjustment) return;

            decimal newInterestRate = 0;
            int pivot = currentMonth - 1 - MonthsBetweenRateAdjustments;



            if (pivot % MonthsBetweenRateAdjustments == 0)
            {
                newInterestRate = Math.Min(IndexRate + Margin, MaximumRate);
                newInterestRate = Math.Max(newInterestRate, MinimumRate);

                decimal rateAdj = newInterestRate - InterestRate;
                rateAdj = rateAdj <0 ? 0 : rateAdj;

                if (rateAdj > MaximumRateAdjustment) newInterestRate = InterestRate + MaximumRateAdjustment;

                InterestRate = newInterestRate;
                _monthlyPayment = Calculator.GetA(_Balance, InterestRate, TermMonths - (currentMonth-1));
                _monthlyInterestRate = InterestRate / 12 / 100;
            }

            

        }

        public override List<SchedulePayment> Calculate()
        {
            List<SchedulePayment> SchedulePayments = new List<SchedulePayment>();
            SchedulePayment payment;
            _Balance = LoanAmount;
            _monthlyPayment = Calculator.GetA(LoanAmount, InterestRate, TermMonths);
            _monthlyInterestRate = InterestRate / 12 / 100;

            for (int i = 0; i < TermMonths; i++)
            {
                AdjustIndexRate(i+1);
                AdjustInterestRate(i + 1);                

                payment = new SchedulePayment()
                {
                    Month = i + 1,
                    MonthlyPayment = _monthlyPayment,
                    InterestPaid = i == 0 ? LoanAmount * _monthlyInterestRate : SchedulePayments[i - 1].Balance * _monthlyInterestRate,
                };

                payment.CumulativeInterest = i == 0 ? payment.InterestPaid : SchedulePayments[i - 1].CumulativeInterest + payment.InterestPaid;
                payment.PrincipalPaid = _monthlyPayment - payment.InterestPaid;
                _Balance =  i == 0 ? LoanAmount - payment.PrincipalPaid : SchedulePayments[i - 1].Balance - payment.PrincipalPaid;
                payment.Balance = _Balance;

                SchedulePayments.Add(payment);
            }

            return SchedulePayments;
        }

    }
    

    public class SchedulePaymentTable {

        public List<SchedulePayment> SchedulePayments { get; set; }
        public ISchedulePaymentCalculator Calculator { get; set; }
             

        public void BuildSchedulePayment() {

            SchedulePayments = Calculator.Calculate();
                        
        }


    }
}
