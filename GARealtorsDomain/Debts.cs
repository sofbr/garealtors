﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class Debts
    {

        public decimal AutoLoans { get; set; }
        public decimal StudentLoans { get; set; }
        public decimal RentalPropertyLoans { get; set; }
        public decimal OtherPayments { get; set; }

        public decimal MonthlyAlimony { get; set; }
        public decimal MonthlyCreditCardPayments { get; set; }

        public decimal TotalDebts { get { return AutoLoans + StudentLoans + RentalPropertyLoans + OtherPayments + MonthlyAlimony + MonthlyCreditCardPayments; } }

    }
}
