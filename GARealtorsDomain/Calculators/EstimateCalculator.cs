﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{

    public enum eEstimateType { Conservative,Aggresive }

    public class EstimateCalculator
    {

        public eEstimateType EstimateType { get; set; }

        public EstimateCalculator(eEstimateType estimateType) {
            EstimateType = estimateType;
        }

        public MonthlyIncome monthlyIncome { get; set; }
        public LoanTerms loanTerms { get; set; }

        public Debts MonthlyPayments { get; set; }

        public MonthlyPayment LoanPayments { get; protected set; }
               
        FixedRateLoanCalculator loanPayment;     

        public void CalculatePayments(decimal DownPaymentPercent) {
            decimal _monthlyPayment = Calculator.GetMonthlyPayment(monthlyIncome.TotalIncome, MonthlyPayments.TotalDebts, DownPaymentPercent, EstimateType);

            FixedRateLoanCalculator _loanCalc = new FixedRateLoanCalculator() { LoanTerms = loanTerms };
            
            decimal _tempMonthlyPayment = 1;

            loanTerms.LoanAmount = 0;
            loanTerms.DownPayment = DownPaymentPercent;
            loanTerms.AppraisedValue = 0;
            loanTerms.MortgagePercent = _loanCalc.GetMortgagePercent();
            int increment=100000;

            decimal AP = Calculator.GetAP(loanTerms.InterestRate, loanTerms.TermMonths);
            decimal _tempAP=1;

            if (loanTerms.InterestRate == 0) {                           

                while ((_monthlyPayment != _tempMonthlyPayment) && increment != 0)
                {

                    loanTerms.LoanAmount += increment;

                    if (loanTerms.LoanAmount == 0) break;
                  
                    _tempMonthlyPayment = Calculator.GetMonthlyPayment(loanTerms.LoanAmount, loanTerms.TotalTaxesExpected, loanTerms.MortgagePercent, loanTerms.InterestRate, loanTerms.TermMonths);

                    if (_tempMonthlyPayment > _monthlyPayment && increment > 0) increment = (increment / 10) * -1;

                    if (_tempMonthlyPayment < _monthlyPayment && increment < 0) increment = (increment / 10) * -1;

                }
            }
            else {
                //Seek for loan amount       
                while ((AP != _tempAP && _monthlyPayment != _tempMonthlyPayment) && increment != 0)
                {
                    loanTerms.LoanAmount += increment;

                    if (loanTerms.LoanAmount == 0) break;

                    _tempAP = Calculator.GetAP(_monthlyPayment, loanTerms.TotalTaxesExpected, loanTerms.MortgagePercent, loanTerms.LoanAmount);
                    _tempMonthlyPayment = Calculator.GetMonthlyPayment(loanTerms.LoanAmount, loanTerms.TotalTaxesExpected, loanTerms.MortgagePercent, loanTerms.InterestRate, loanTerms.TermMonths);

                    if (_tempMonthlyPayment > _monthlyPayment && increment > 0) increment = (increment / 10) * -1;

                    if (_tempMonthlyPayment < _monthlyPayment && increment < 0) increment = (increment / 10) * -1;

                }
            }


           

            _loanCalc.Calculate();
                                 
            LoanPayments = _loanCalc.MonthlyPayment;           

        }

      
    }
}
