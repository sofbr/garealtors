﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class Calculator
    {
        // A = Periodic payment amount
        // P = Amount of principal
        // i = Periodic interest rate
        // n = Total number of payments

        //
        // A/P = i*(1+i)^n / (1+i)^n - 1
        //

        public eEstimateType EstimateType { get; set; }
      

        protected static double GetExp(decimal i, decimal termMonths){
            return Math.Pow( 1 + (double)i,(double)termMonths);
        }

        /// <summary>
        /// Gets the expression A/P = i(1+i)^n / (1+i)^n - 1       
        /// </summary>
        /// <param name="i">Interest rate</param>
        /// <param name="termMonths">Loan term in months</param>
        /// <returns>AP value</returns>
        public static decimal GetAP(decimal i, int termMonths){

            if (i == 0) return 0;

            i = i / 100 / 12;
            decimal exp = (decimal)GetExp(i,termMonths);

            return (i*exp)/(exp-1);
        }

        public static decimal GetAP(decimal monthlyPayment, decimal taxesExpected, decimal anualMortgageInsurance, decimal P) {

            return (monthlyPayment - (taxesExpected/12) - ( ((anualMortgageInsurance/100) * P) / 12)) / P;

        }

        public static decimal GetMonthlyPayment(decimal P,decimal taxesExpected,decimal anualMortgageInsurance,decimal interestRate,int termMonths) {

            decimal A = GetA(P, interestRate, termMonths);
            decimal taxes = taxesExpected/12;
            decimal mortgageInsuranceValue = (anualMortgageInsurance/100 * P) / 12;

            return A + taxes + mortgageInsuranceValue;

        }

        public static decimal GetMonthlyPayment(decimal totalIncome, decimal totalDebts, decimal downPayment, eEstimateType estimateType)
        {

            decimal housingRatio = 0;
            decimal debtRatio = 0;

            if (estimateType == eEstimateType.Conservative)
            {
                housingRatio = downPayment < 20 ? totalIncome * 0.28m : totalIncome * 0.30m;
                debtRatio = (totalIncome * 0.36m) - totalDebts;
            }
            else if (estimateType == eEstimateType.Aggresive)
            {

                if (downPayment >= 0 && downPayment < 10) {
                    housingRatio = totalIncome * 0.3m;
                    debtRatio = (totalIncome * 0.38m) - totalDebts;
                }
                else if (downPayment >= 10 && downPayment < 20) {
                    housingRatio = totalIncome * 0.33m;
                    debtRatio = (totalIncome * 0.40m) - totalDebts;                
                }
                else if (downPayment >= 20) {
                    housingRatio = totalIncome * 0.36m;
                    debtRatio = (totalIncome * 0.42m) - totalDebts;                                
                
                }

            }

            var monthlyPayment = housingRatio < debtRatio ? housingRatio : debtRatio;

            return monthlyPayment < 0 ? 0 : monthlyPayment;
        }

        /// <summary>
        /// Get principal amount of a loan
        /// </summary>
        /// <param name="A">The monthly payment</param>
        /// <param name="i">Interest rate</param>
        /// <param name="termMonths">Loan term in months</param>
        /// <returns>Principal amount</returns>
        public static decimal GetP(decimal A,decimal i,int termMonths) {            
            var AP = GetAP(i, termMonths);

            return A / AP;
        }

        /// <summary>
        /// Gets monthly payment
        /// </summary>
        /// <param name="P">Principal Amount</param>
        /// <param name="i">Anual Interest Rate</param>
        /// <param name="termMonths">Loan term in months</param>
        /// <returns>Loan monthly payments</returns>
        public static decimal GetA(decimal P,decimal i,int termMonths) {

            if (i == 0) return P / termMonths;

            var AP=GetAP(i, termMonths);

            return P * AP;
        }

       

    }
}
