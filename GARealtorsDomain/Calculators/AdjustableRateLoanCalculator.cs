﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator.MortgageCalculator
{
    public class AdjustableRateLoanCalculator
    {

        public LoanTerms LoanTerms { get; set; }
        public MonthlyPayment MonthlyPayment { get; set; }

    }
}
