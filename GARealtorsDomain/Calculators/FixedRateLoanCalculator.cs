﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class FixedRateLoanCalculator
    {

        public LoanTerms LoanTerms { get; set; }
        public MonthlyPayment MonthlyPayment { get; set; }
                         
        public decimal GetTaxesExpected() {
            return LoanTerms.TotalTaxesExpected / 12;
        }

        public decimal GetMortgagePercent() {

            //If loan months is greater than 288 (24 years)
            if (LoanTerms.DownPayment < 20m)
            {

                if (LoanTerms.TermMonths > 288)
                {
                    if (LoanTerms.DownPayment >= 0 && LoanTerms.DownPayment < 5) return 1.0984m;
                    if (LoanTerms.DownPayment >= 5 && LoanTerms.DownPayment < 10) return 0.9416m;
                    if (LoanTerms.DownPayment >= 10 && LoanTerms.DownPayment < 15) return 0.62m;
                    if (LoanTerms.DownPayment >= 15 && LoanTerms.DownPayment < 20) return 0.3808m;
                }
                else {
                    if (LoanTerms.DownPayment >= 0 && LoanTerms.DownPayment < 5) return 0.9904m;
                    if (LoanTerms.DownPayment >= 5 && LoanTerms.DownPayment < 10) return 0.8313m;
                    if (LoanTerms.DownPayment >= 10 && LoanTerms.DownPayment < 15) return 0.5097m;
                    if (LoanTerms.DownPayment >= 15 && LoanTerms.DownPayment < 20) return 0.2729m;                                        
                }

                return 0;

            }

            else return 0;
        }

        public decimal GetAdjustableMortgagePercent()
        {

            //If loan months is greater than 288 (24 years)
            if (LoanTerms.DownPayment < 20m)
            {

                if (LoanTerms.TermMonths > 288)
                {
                    if (LoanTerms.DownPayment >= 0 && LoanTerms.DownPayment < 5) return 1.3575m;
                    if (LoanTerms.DownPayment >= 5 && LoanTerms.DownPayment < 10) return 1.0800m;
                    if (LoanTerms.DownPayment >= 10 && LoanTerms.DownPayment < 15) return 0.7800m;
                    if (LoanTerms.DownPayment >= 15 && LoanTerms.DownPayment < 20) return 0.4376m;
                }
                else
                {
                    if (LoanTerms.DownPayment >= 0 && LoanTerms.DownPayment < 5) return 1.2480m;
                    if (LoanTerms.DownPayment >= 5 && LoanTerms.DownPayment < 10) return 0.9726m;
                    if (LoanTerms.DownPayment >= 10 && LoanTerms.DownPayment < 15) return 0.6733m;
                    if (LoanTerms.DownPayment >= 15 && LoanTerms.DownPayment < 20) return 0.3317m;
                }

                return 0;

            }

            else return 0;
        }

        public decimal GetMortgageInsurance() {

            LoanTerms.MortgagePercent = GetMortgagePercent();
            return (LoanTerms.LoanAmount * (LoanTerms.MortgagePercent / 100)) / 12;
           
        }

        public decimal GetAdjustableMortgageInsurance()
        {

            LoanTerms.MortgagePercent = GetAdjustableMortgagePercent();
            return (LoanTerms.LoanAmount * (LoanTerms.MortgagePercent / 100)) / 12;

        }

        public void Calculate() {
          
            MonthlyPayment= new MonthlyPayment();

            MonthlyPayment.PriceOfHome = LoanTerms.AppraisedValue;
            //if (LoanTerms.DownPayment == 0) LoanTerms.DownPayment = ((LoanTerms.AppraisedValue - LoanTerms.LoanAmount)*100) / LoanTerms.AppraisedValue;
            
            MonthlyPayment.DownPaymentPercent = LoanTerms.DownPayment;
            MonthlyPayment.LoanAmount = LoanTerms.LoanAmount;

            if(LoanTerms.AppraisedValue==0) MonthlyPayment.PriceOfHome = LoanTerms.LoanAmount / (1 - (LoanTerms.DownPayment / 100));
            MonthlyPayment.DownPaymentAmount = MonthlyPayment.PriceOfHome - MonthlyPayment.LoanAmount;
           
            MonthlyPayment.PrincipalAndInterest = Calculator.GetA(LoanTerms.LoanAmount,LoanTerms.InterestRate,LoanTerms.TermMonths);
            MonthlyPayment.TaxesAndInsurance = GetTaxesExpected();           
            MonthlyPayment.MortgageInsurance = GetMortgageInsurance();            

        }

        public void CalculateAdjustable()
        {

            MonthlyPayment = new MonthlyPayment();

            MonthlyPayment.PriceOfHome = LoanTerms.AppraisedValue;
            //if (LoanTerms.DownPayment == 0) LoanTerms.DownPayment = ((LoanTerms.AppraisedValue - LoanTerms.LoanAmount)*100) / LoanTerms.AppraisedValue;

            MonthlyPayment.DownPaymentPercent = LoanTerms.DownPayment;
            MonthlyPayment.LoanAmount = LoanTerms.LoanAmount;

            if (LoanTerms.AppraisedValue == 0) MonthlyPayment.PriceOfHome = LoanTerms.LoanAmount / (1 - (LoanTerms.DownPayment / 100));
            MonthlyPayment.DownPaymentAmount = MonthlyPayment.PriceOfHome - MonthlyPayment.LoanAmount;

            MonthlyPayment.PrincipalAndInterest = Calculator.GetA(LoanTerms.LoanAmount, LoanTerms.InterestRate, LoanTerms.TermMonths);
            MonthlyPayment.TaxesAndInsurance = GetTaxesExpected();
            MonthlyPayment.MortgageInsurance = GetAdjustableMortgageInsurance();

        }
      


    }
}
