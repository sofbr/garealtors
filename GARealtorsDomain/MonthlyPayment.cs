﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class MonthlyPayment
    {
        public decimal DownPaymentPercent { get; set; }
        public decimal DownPaymentAmount { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal PriceOfHome { get; set; }

        public decimal PrincipalAndInterest { get; set; }
        public decimal TaxesAndInsurance { get; set; }
        public decimal MortgageInsurance { get; set; }

        public decimal TotalPayment { get { return PrincipalAndInterest + TaxesAndInsurance + MortgageInsurance; } }


    }
}
