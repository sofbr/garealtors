﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculator
{
    public class LoanTerms
    {
        public int HousingRatio { get; set; }
        public decimal MortgagePercent { get; set; }

        public decimal LoanAmount { get; set; }
        public decimal AppraisedValue { get; set; }

        public decimal InterestRate { get; set; }
        public int TermYears { get; set; }
        public int TermMonths { get { return TermYears * 12; } }
        public decimal DownPayment { get; set; }

        public decimal YearlyPropertyTax { get; set; }
        public decimal YearlyPropertyInsurance { get; set; }
        public decimal TotalTaxesExpected { get { return YearlyPropertyTax + YearlyPropertyInsurance; } }

    }
}
